﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person.Interfaces
{
    public interface IExpectSurname
    {
        IExpectPrimaryContact WithSurname(string surname);
    }
}