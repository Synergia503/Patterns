﻿using AbstractFactoryDemo.Interfaces;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person.Interfaces
{
    public interface IExpectPrimaryContact
    {
        IExpectAlternateContact WithPrimaryContact<T>(IBuildingSpecification<T> primaryContactSpec) where T : IContactInfo;
    }
}