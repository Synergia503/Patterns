﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person.Interfaces
{
    public interface IExpectName
    {
        IExpectSurname WithName(string name);
    }
}