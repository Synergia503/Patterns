﻿using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person
{
    public class PersonSpecification :
        IExpectName, IExpectSurname,
        IExpectPrimaryContact, IExpectAlternateContact,
        IBuildingSpecification<Models.Person>
    {
        private string Name { get; set; }
        private string Surname { get; set; }
        private IEnumerable<IBuildingSpecification<IContactInfo>> ContactSpecs { get; set; }

        // Difference between Builder Pattern and Specificaiton Pattern - Builder is holding properties to real object (entire parts), and Specification is holding specificaiton parts
        private IBuildingSpecification<IContactInfo> PrimaryContactSpec { get; set; }

        private PersonSpecification() { }

        public static IExpectName Initialize() => new PersonSpecification();

        public IExpectSurname WithName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException();

            return new PersonSpecification()
            {
                Name = name
            };
        }

        public IExpectPrimaryContact WithSurname(string surname)
        {
            if (string.IsNullOrEmpty(surname))
                throw new ArgumentException();

            return new PersonSpecification()
            {
                Name = Name,
                Surname = surname
            };
        }

        public IExpectAlternateContact WithPrimaryContact<T>(IBuildingSpecification<T> primaryContactSpec) where T : IContactInfo
        {
            if (primaryContactSpec == null)
                throw new ArgumentNullException();

            var convertedSpec = new ConvertingSpecification<IContactInfo, T>(primaryContactSpec);

            return new PersonSpecification()
            {
                Name = Name,
                Surname = Surname,
                ContactSpecs = new[] { convertedSpec },
                PrimaryContactSpec = convertedSpec
            };
        }

        public IExpectAlternateContact WithAlternateContact<T>(IBuildingSpecification<T> contactSpec) where T : IContactInfo
        {
            if (contactSpec == null)
                throw new ArgumentNullException();

            var convertedSpec = new ConvertingSpecification<IContactInfo, T>(contactSpec);

            return new PersonSpecification()
            {
                Name = Name,
                Surname = Surname,
                ContactSpecs = new List<IBuildingSpecification<IContactInfo>>(ContactSpecs) { convertedSpec },
                PrimaryContactSpec = PrimaryContactSpec
            };
        }

        public bool CanAdd<T>(IBuildingSpecification<T> contactSpec) where T : IContactInfo =>
          contactSpec != null &&
          CanAdd(new ConvertingSpecification<IContactInfo, T>(contactSpec));

        public IBuildingSpecification<Models.Person> AndNoMoreContacts() => this;

        // At this point there will be no exceptions.
        // Even in the subordinate Build() methods there will be no exceptions, because all validation have been already performed!
        public Models.Person Build() =>
            new Models.Person()
            {
                Name = Name,
                LastName = Surname,
                PrimaryContact = PrimaryContactSpec.Build(), // Specification Pattern behaves like a multilevel Builder. Person is one level of complexity, but it contains more level beneath it.
                ContactsList = ContactSpecs.Select(spec => spec.Build()).ToList()
            };


        // Specification is now testable by equality. But this is third responsibility. CanAdd() is better.
        public bool Equals(IBuildingSpecification<Models.Person> other) =>
           Equals(other as PersonSpecification);

        private bool Equals(PersonSpecification other) =>
            other != null &&
            Name == other.Name &&
            Surname == other.Surname &&
            PrimaryContactSpec.SafeEquals(other.PrimaryContactSpec) &&
            ContactSpecs.SequenceEqual(other.ContactSpecs);
    }
}