﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Producer
{
    public class ProducerSpecification : IBuildingSpecification<Models.Producer>
    {
        private string Name { get; set; }

        private ProducerSpecification() { }

        public static IBuildingSpecification<Models.Producer> WithName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException();

            return new ProducerSpecification()
            {
                Name = name
            };
        }

        public Models.Producer Build() =>
            new Models.Producer() { Name = Name };

        public bool Equals([AllowNull] IBuildingSpecification<Models.Producer> other)
        {
            throw new NotImplementedException();
        }
    }
}