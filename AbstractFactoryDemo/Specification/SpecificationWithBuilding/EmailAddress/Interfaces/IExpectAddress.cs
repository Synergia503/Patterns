﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.EmailAddress.Interfaces
{
    public interface IExpectAddress
    {
        IBuildingSpecification<Models.EmailAddress> WithAddress(string emailAddress);
    }
}