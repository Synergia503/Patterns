﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.PhoneNumber.Interfaces
{
    public interface IExpectAreaCode
    {
        IExpectNumber WithAreaCode(int areaCode);
    }
}