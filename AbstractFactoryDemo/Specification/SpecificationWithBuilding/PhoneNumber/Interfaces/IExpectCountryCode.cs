﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.PhoneNumber.Interfaces
{
    public interface IExpectCountryCode
    {
        IExpectAreaCode WithCountryCode(int countryCode);
    }
}