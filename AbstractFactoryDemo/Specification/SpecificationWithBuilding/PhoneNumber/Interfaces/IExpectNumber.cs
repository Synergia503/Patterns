﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.PhoneNumber.Interfaces
{
    public interface IExpectNumber
    {
        IBuildingSpecification<Models.PhoneNumber> WithNumber(int number);
    }
}