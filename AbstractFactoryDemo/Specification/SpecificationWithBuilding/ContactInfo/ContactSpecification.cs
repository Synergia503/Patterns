﻿using AbstractFactoryDemo.Specification.SpecificationWithBuilding.EmailAddress;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.PhoneNumber;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.PhoneNumber.Interfaces;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.ContactInfo
{
    public static class ContactSpecification
    {
        public static IBuildingSpecification<Models.EmailAddress> ForEmailAddress(string emailAddress) =>
            EmailAddressSpecification.Initialize().WithAddress(emailAddress);

        public static IExpectCountryCode ForPhoneNumber() =>
            PhoneNumberSpecification.Initialize();
    }
}