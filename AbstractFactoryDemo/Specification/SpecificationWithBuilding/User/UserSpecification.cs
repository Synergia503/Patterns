﻿using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Machine;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Machine.Interfaces;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Person.Interfaces;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.User
{
    public static class UserSpecification
    {
        public static IExpectName ForPerson() => PersonSpecification.Initialize();
        public static IExpectProducer ForMachine() => MachineSpecification.Initialize();
    }
}