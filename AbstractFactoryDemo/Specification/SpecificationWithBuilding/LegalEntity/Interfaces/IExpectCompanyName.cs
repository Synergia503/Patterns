﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.LegalEntity.Interfaces
{
    public interface IExpectCompanyName
    {
        IExpectEmailAddress WithCompanyName(string companyName);
    }
}