using AbstractFactoryDemo.Interfaces;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.LegalEntity.Interfaces
{
    public interface IExpectOtherContact
    {
        IExpectOtherContact WithOtherContact(IBuildingSpecification<IContactInfo> contactSpec);
        IBuildingSpecification<Models.LegalEntity> AndNoMoreContacts();
    }
}