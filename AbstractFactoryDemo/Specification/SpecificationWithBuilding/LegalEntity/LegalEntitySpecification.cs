﻿using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.LegalEntity.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.LegalEntity
{
    public class LegalEntitySpecification :
        IExpectCompanyName, IExpectEmailAddress,
        IExpectPhoneNumber, IExpectOtherContact,
        IBuildingSpecification<Models.LegalEntity>
    {
        private string CompanyName { get; set; }
        private IBuildingSpecification<Models.EmailAddress> EmailAddressSpec { get; set; }
        private IBuildingSpecification<Models.PhoneNumber> PhoneNumberSpec { get; set; }

        private IList<IBuildingSpecification<IContactInfo>> OtherContactSpecs { get; set; }
            = new List<IBuildingSpecification<IContactInfo>>();

        private LegalEntitySpecification() { }

        public static IExpectCompanyName Initialize() => new LegalEntitySpecification();

        public IExpectEmailAddress WithCompanyName(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
                throw new ArgumentException();

            return new LegalEntitySpecification()
            {
                CompanyName = companyName
            };
        }

        public IExpectPhoneNumber WithEmailAddress(IBuildingSpecification<Models.EmailAddress> emailSpec)
        {
            if (emailSpec == null)
                throw new ArgumentNullException();

            return new LegalEntitySpecification()
            {
                CompanyName = CompanyName,
                EmailAddressSpec = emailSpec
            };
        }

        public IExpectOtherContact WithPhoneNumber(IBuildingSpecification<Models.PhoneNumber> phoneSpec)
        {
            if (phoneSpec == null)
                throw new ArgumentNullException();

            return new LegalEntitySpecification()
            {
                CompanyName = CompanyName,
                EmailAddressSpec = EmailAddressSpec,
                PhoneNumberSpec = phoneSpec
            };
        }

        public IExpectOtherContact WithOtherContact(IBuildingSpecification<IContactInfo> contactSpec)
        {
            if (contactSpec == null)
                throw new ArgumentNullException();

            return new LegalEntitySpecification()
            {
                CompanyName = CompanyName,
                EmailAddressSpec = EmailAddressSpec,
                PhoneNumberSpec = PhoneNumberSpec,
                OtherContactSpecs = new List<IBuildingSpecification<IContactInfo>>(OtherContactSpecs) { contactSpec }
            };
        }

        public IBuildingSpecification<Models.LegalEntity> AndNoMoreContacts() => this;

        public Models.LegalEntity Build() =>
            new Models.LegalEntity()
            {
                CompanyName = CompanyName,
                EmailAddress = EmailAddressSpec.Build(),
                PhoneNumber = PhoneNumberSpec.Build(),
                OtherContacts = OtherContactSpecs.Select(spec => spec.Build()).ToList()
            };

        public bool Equals([AllowNull] IBuildingSpecification<Models.LegalEntity> other)
        {
            throw new NotImplementedException();
        }
    }
}