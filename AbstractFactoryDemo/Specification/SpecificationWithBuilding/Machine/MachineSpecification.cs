﻿using System;
using System.Diagnostics.CodeAnalysis;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Machine.Interfaces;

namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Machine
{
    public class MachineSpecification :
        IExpectProducer, IExpectModel, IExpectOwner,
        IBuildingSpecification<Models.Machine>
    {
        private IBuildingSpecification<Models.Producer> ProducerSpec { get; set; }
        private string Model { get; set; }
        private IBuildingSpecification<Models.LegalEntity> OwnerSpec { get; set; }

        private MachineSpecification() { }

        public static IExpectProducer Initialize() => new MachineSpecification();

        public IExpectModel ProducedBy(IBuildingSpecification<Models.Producer> producerSpec)
        {
            if (producerSpec == null)
                throw new ArgumentNullException();

            return new MachineSpecification()
            {
                ProducerSpec = producerSpec
            };
        }

        public IExpectOwner WithModel(string model)
        {
            if (string.IsNullOrEmpty(model))
                throw new ArgumentException();

            return new MachineSpecification()
            {
                ProducerSpec = ProducerSpec,
                Model = model
            };
        }

        public IBuildingSpecification<Models.Machine> OwnedBy(IBuildingSpecification<Models.LegalEntity> ownerSpec)
        {
            if (ownerSpec == null)
                throw new ArgumentNullException();

            return new MachineSpecification()
            {
                ProducerSpec = ProducerSpec,
                Model = Model,
                OwnerSpec = ownerSpec
            };
        }

        public Models.Machine Build() =>
            new Models.Machine()
            {
                Producer = ProducerSpec.Build(),
                Model = Model,
                Owner = OwnerSpec.Build()
            };

        public bool Equals([AllowNull] IBuildingSpecification<Models.Machine> other)
        {
            throw new NotImplementedException();
        }
    }
}