﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Machine.Interfaces
{
    public interface IExpectProducer
    {
        IExpectModel ProducedBy(IBuildingSpecification<Models.Producer> producerSpec);
    }
}