﻿namespace AbstractFactoryDemo.Specification.SpecificationWithBuilding.Machine.Interfaces
{
    public interface IExpectModel
    {
        IExpectOwner WithModel(string model);
    }
}