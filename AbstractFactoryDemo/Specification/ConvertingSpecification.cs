﻿using System;

namespace AbstractFactoryDemo.Specification
{
    // This is class was added to preserve "covariance" behaviour
    // This class imitates/simulates the 'out' keyword used in generic types.
    public class ConvertingSpecification<TBase, TProduct> : IBuildingSpecification<TBase> where TProduct : TBase
    {
        private IBuildingSpecification<TProduct> ContainedSpec { get; }

        public ConvertingSpecification(IBuildingSpecification<TProduct> containedSpec)
        {
            ContainedSpec = containedSpec ?? throw new ArgumentNullException();
        }

        // This line compiles because it is based on plain old Substitution Principle. In other words this class reduced out to the SP. 
        public TBase Build() => ContainedSpec.Build();

        public bool Equals(IBuildingSpecification<TBase> other)
        {
            return other is ConvertingSpecification<TBase, TProduct> convertingSpec && ContainedSpec.Equals(convertingSpec.ContainedSpec);
        }

        public override int GetHashCode() => ContainedSpec.GetHashCode();

        public override bool Equals(object obj) =>
            Equals(obj as IBuildingSpecification<TBase>) || ContainedSpec.Equals(obj as IBuildingSpecification<TProduct>);
    }
}