﻿using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Models;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.ContactInfo;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.LegalEntity;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.Producer;
using AbstractFactoryDemo.Specification.SpecificationWithBuilding.User;
using AbstractFactoryDemo.Specification.Validation;
using System;
using System.Linq;

namespace AbstractFactoryDemo.Specification
{
    // Used for creating entire and complex graph object in one single operation
    // Specification is good for situation, when number of related classes of Domain Model grew large.
    // For creating Person Object the Builder Pattern will be the best here.
    // Specification added depth to the Builder.
    // Instead making the Builder with thousands lines of code, we made a lot smaller Specification classes, each dealing with one class in the Domain Model.
    // So if Builder is growing, consider refactoring it into Specification.
    class SpecificationPattern
    {
        // One of the most powerfull and flexible Design Patterns 
        internal static void Sample()
        {
            // Displaying in user interface
            Person person = new Person() { Name = "Max", LastName = "Planck" };
            DoSomethingWith(person, p =>
            // those conditions complete the user interface SPECIFICATION.
            // this predicate precisely defines what conditions must be satisfied by the object before an operation can be performed on that object.
            p.Name != null &&
            p.LastName != null &&
            p.Contacts != null &&
            (p.PrimaryContact == null ||
            p.Contacts.Contains(p.PrimaryContact))
            );

            // Saving to DB
            DoSomethingWith(person, p =>
            // There are slight differences between two specifications.
            // That is exactly what it means to provide extensibility through specifications.
            !string.IsNullOrEmpty(p.Name) &&
            !string.IsNullOrEmpty(p.LastName) &&
            p.Contacts != null &&
            p.PrimaryContact != null &&
            p.Contacts.Contains(p.PrimaryContact)
            );

            // Method which displays the validation message is the same in both cases. Only the two specifications are different.
            // Different parts of the system will come up with different specification objects. 
            // Persistance layer would have the specification which says which object can and which object cannot be stored to the DB.
            // Displaying layer would have its own specification which says which object can and which can't be displayed to the user.
            // The point - The Person Object would never know whether it is good or not. This is the good thing, because the Person Object doesn't know the context in which it is used. Is it good for what?
            // Person Object itselft cannot provide the answer.
        }

        static void DoSomethingWith(Person person, Func<Person, bool> predicate)
        {
            if (!predicate(person))
            {
                return;
            }

            // With predicate in this method you can easily extend it. (OCP is obeyed), We can change what the method is doing by applying different predicates ( to be precise - what the method is checking before doing it)
            // 1. Console writeline OR
            // 2. Save to DB OR
            // 3. Send HTTP Request OR
            // 4. Send queue message OR etc. etc
        }
    }


    // Specification Pattern comes with two important traits. 
    // 1. The flexibility in building expressions trees.
    // 2. An ability for human eyes to read those expression trees.

    // Specification Pattern comes with one benefit that we don't have with delegates.
    // It is an object. And as an object it can be persisted to the DB. Or reconstructed from the recovery file, or send over the network/queue etc. etc.
    class SpecificationAsExpressionTree
    {
        internal static void Sample()
        {
            // Displaying in user interface
            Person person = new Person() { Name = "Max", LastName = "Planck" };
            DoSomethingWith(
                person,
                // This is probably the ugliest part of the code throughout this course. Nevertheless it specifies the requirement, that the Name property of the Person object should not be null.
                // One technique (with low cost and high return) for figthing against language syntax - wrapping it into Utility class.
                //new Property<Person, string>(p => p.Name, new NotNull<string>())
                // since both Name and LastName have to be NotNull, All() method has to be used here.
                //new Composite<Person>(results => results.All(singleResult => singleResult),
                //Spec<Person>.NotNull<string>(p => p.Name), // Better this way (with static Spec class)
                //Spec<Person>.NotNull<string>(p => p.LastName))

                // even more readable approach 
                Spec<Person>.NotNull<string>(p => p.Name)
                .And(Spec<Person>.NotNull<string>(p => p.LastName))
                .And(Spec<Person>.NotNull(p => p.Contacts))
                .And(
                    Spec<Person>.Null(p => p.PrimaryContact)
                    .Or(Spec<Person>.IsTrue(p => p.Contacts.Contains(p.PrimaryContact))))
                );

            // Saving to DB
            // Here should be specifications for saving to the DB.
            DoSomethingWith(person, null);
        }

        static void DoSomethingWith(Person person, Specification<Person> personSpecification)
        {
            if (!personSpecification.IsSatisfiedBy(person))
            {
                return;
            }

            // With predicate in this method you can easily extend it. (OCP is obeyed), We can change what the method is doing by applying different predicates ( to be precise - what the method is checking before doing it)
            // 1. Console writeline OR
            // 2. Save to DB OR
            // 3. Send HTTP Request OR
            // 4. Send queue message OR etc. etc
        }
    }

    public class SpecificationWithBuilder
    {
        static void Sample()
        {
            // This example is far away from something realistic, it's just showing the principle.
            IUser user =
                UserSpecification
                .ForPerson()
                .WithName("Max")
                .WithSurname("Planck")
                .WithPrimaryContact(
                    ContactSpecification.ForEmailAddress("")
                    )
                .WithAlternateContact(
                    ContactSpecification
                    .ForPhoneNumber()
                    .WithCountryCode(1)
                    .WithAreaCode(23)
                    .WithNumber(123123123)
                    )
                .AndNoMoreContacts()
                .Build();

            // Any of those Specificaitons can come from DB, not be hardcored.
            IUser machine =
              UserSpecification
                  .ForMachine()
                  .ProducedBy( // this is different than in Builder Pattern. This method does not require Producer Object but it requires Producer Specification.
                      ProducerSpecification
                          .WithName("Big Co.")) // WithName() method returns IBuildingSpecification interface, not real object. So this interface only shows, 
                                                // how the real object should like one day, when client decides to build it.
                  .WithModel("Shiny one")
                  .OwnedBy(
                      LegalEntitySpecification
                          .Initialize()
                          .WithCompanyName("Properties Co.")
                          .WithEmailAddress(
                              ContactSpecification.ForEmailAddress("one@prop"))
                          .WithPhoneNumber(
                              ContactSpecification
                                  .ForPhoneNumber()
                                  .WithCountryCode(1)
                                  .WithAreaCode(23)
                                  .WithNumber(456))
                          //.WithOtherContact(
                          //ContactSpecification.ForEmailAddress("two@prop")) // Commented out because of the not implemented ConvertingSpecification in that WithOtherContact() method.
                          .AndNoMoreContacts())
                  .Build();
            // Caller won't be able to provide any incomplete subobject (unacceptable piece of information), or forget build some mandatory parts of the object and continue. 
            // This is most valuable - incorrent code won't compile - when dealing with very complex objects.


            // This is the difference betweend Builder Pattern and Specification Pattern - SP is dealing with the tree of complex objects. 
            // Although we know that is easy to specify an email address, let's pretend it is not
            // Let's assume that Email Object has its own huge complexity and that passing entire object as an argument will be inappriopriate.
            // Specification Pattern acknowledges, that every contained object may come with its own deep and complex hierachy.
            // For that reason, methods of the Specification Object would not ask for other objects, but instead, for other Specificaitons. 
            // We don't have to know (and this is the good thing) where those specificaitons are coming from, they can be constructed on the fly. Or they could come out from the DB, or from the network.
            // Specification is AN OBJECT. It can be persisted in the DB, defined in the XML file, can be serialized and sent. 
            // This is power of the SP, use that power when you need it. When you face really complex objects use the haviest axe you have on your disposal - SP.
        }

        // What is the benefit of using Specification Pattern to construct the Person object.
        // If Person object would come with much more object, each of them with much larger complexity (with some legal/business rules)
        // The point is that parts of this code could be represented by Specificaiton objects that come from other sources. And parts would be constructed in those places
        // Contact Specification could come as method arguments. We don't care when and how those objects have been created.
        // We don't have to know, whether their content comes from UI, DB or network
    }

    public class DealingWithIncompleteObjects
    {
        // This is one of the biggest issue and common problem in programming.
        // Complex object often requires several steps to construct.
        // This can include extern system, remote user, or anything else what is promptly available, then we meet this situation, when incomplete object must be somehow persisted before continuing.
        // What to do? Should incomplete object be stored in DB? Bad idea - There could be need for changing DB schema for instance for accepting NULLs or for allowing some FK to be missing (or dropping some else constraints).
        // More problems - incomplete objects should be invisible to most of the operations. Or there must exist a way (logic in code) of telling whether object is complete or not (could be done with SPattern).
        // We don't have easy way of telling if object is incomplete before loading into memory and applying Specifications into it.

        // Two solutions:
        // 1. Add IsValid column into DB - this is fine because then the valition test can be incorporated in all DB queries. Problem - changing validation rules at some later time. 
        // Any change to the rules means loading all objects into memory, walking through them, and changing IsValid if needed and save this back to the DB.
        // 2. Move validation logic to the DB (for instance stored procedures). That approach carries clear risks. Typically we would need validation rules in the domain model as well.
        // Then it implies that validation logic will be duplicated and held both in DB and Domain Model.

        // Let's turn it into completely different direction.
        // Suggestion - do NOT store incomplete objects in DB, but represent them with Building Specifications. What's the difference?
        // Specification may naturally be incomplete, because of the processes happening.
        // Problem is now much smaller - business queries remain the same and only on the Entity (for instance Person) table. (those queries must not have been implemented on the Specifications)
        // And specification table of some entity represents future objects, which do not exist now.
        // By keeping such objects in the different set of tables means that they will be inaccessible to the usual operation (business queries) by definition.
        // First problem is solved just because it's disappeared.
        // Next problem - when specification can actually construct the object is also solved by disappearing.
        // cause Specification itself tells when the object is valid/is complete or not. 
        // Specification knows if object is complete
        // We can change implementation and rules will change with that. 
        // There is no reason to update any flags in DB.
        // Next time we load the data, the Specificaitons will tell at what state the object is. That is what Specifications are doing.
        // DB tables where we store Specifications can look exactly the same as the tables with actual domain objects.
        // This duplicated Specification tables will come with relaxed constraints. 
        // We are free to let all columns be nullable, all FK can be missing, cause there will be no business queries over that table.
        // Sole purpose is to describe incomplete objects, objects that are yet to be completed, and then move to their permament positition in the set of tables holding real objects.
        // SPattern just serves the purpose of making it possible for us as OO programmers to navigate through this long construction process in terms of objects.
        // This objects are just Specifying the target, domain objects.
        // This constructing process can be done in many days - legal documents, office blueprints, assembling videoclip in cloud service. 
        // All those uses require a long list of things before the final commit.
    }
}