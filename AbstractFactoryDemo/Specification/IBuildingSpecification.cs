﻿using System;

namespace AbstractFactoryDemo.Specification
{
    // Why to keep building process (using Builder Pattern) and validating process (using Specification pattern) separately?
    // What's the purpose of having built object which then fails validation? What should be next then? All validation process would just fail.
    // So the whole building process is overwhelming by its nature - it builds the object which cannot be used then.
    // So why not to merge both processes into one step?
    // This interface will serve that goal. 
    // Object which will implement this interface, will grasp both validation and building an entity as a single concept.
    // The steps will be conducted this way, in such steps/in such order, that the object, which comes out, will satisfy all Specifications out of the box.
    public interface IBuildingSpecification<T> : IEquatable<IBuildingSpecification<T>>
    {
        // Build method will never fail, when designed this way.
        T Build();
    }
}