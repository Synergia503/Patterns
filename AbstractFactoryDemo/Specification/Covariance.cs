﻿namespace AbstractFactoryDemo.Specification
{
    // Stay without covariance means stay without Substitution Principle in generic classes
    class Covariance
    {
        class Cat
        {
            public static implicit operator Cat(Dog dog)
                => new Cat();
        }

        class Dog
        {
            // For debugging purposes
            public Dog()
            {

            }

            internal Cat AsCat() => new Cat();
        }

        class WrappedDog : Cat
        {
            public WrappedDog(Dog dog)
            {

            }

        }

        public void Show()
        {
            Cat cat = new Dog(); // this ofc does not compile withou implicit operator. This will hit first Dog's constructor, then the implicit operator
            Cat cat2 = new Dog().AsCat(); // AsCat is better than implicit or explicit operators. It tells immediatebly what it is doing.
            Cat cat3 = new WrappedDog(new Dog()); // It's even better, now, the Dog class does not have to know about its convertions.
            // Above example can show how to introduce covariance in code when the rules of programming language does not allow it to be achieved.
            // This only example allows to obey OO principles.
        }
    }
}