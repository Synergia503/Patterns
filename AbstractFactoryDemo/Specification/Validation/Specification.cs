﻿using AbstractFactoryDemo.Specification.Validation.Infrastructure;
using System.Linq;

namespace AbstractFactoryDemo.Specification.Validation
{
    // This class does not have/does not propose implementation, it only proposes the structure of the validation process.
    // Good thing about specification is that they can be built from smaller runs.
    public abstract class Specification<T>
    {
        public abstract bool IsSatisfiedBy(T obj);

        // Specificaiton itself is able to end with some other Specification.
        public Specification<T> And(Specification<T> other) =>
            new Composite<T>((results) => results.All(res => res == true), this, other);

        public Specification<T> Or(Specification<T> other) =>
            new Composite<T>((results) => results.Any(res => res == true), this, other);

        // This negate result of this specification.
        public Specification<T> Not() =>
            new Transform<T>(res => !res, this);
    }
}