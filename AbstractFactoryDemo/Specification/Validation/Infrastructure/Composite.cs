﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AbstractFactoryDemo.Specification.Validation.Infrastructure
{
    // Composition is another extension which can be offered to the clients.
    // It will accept the indefinite number of the subordinate Specifications of the same type.
    // The composition function will glue them together.
    // Each of the contained specifications will return the boolean value, then the Composition function will be applied to produce single boolean value out of 
    internal class Composite<T> : Specification<T>
    {
        private Func<IEnumerable<bool>, bool> CompositionFunction { get; }
        private IEnumerable<Specification<T>> Subspecifications { get; }

        public Composite(Func<IEnumerable<bool>, bool> compositionFunction, params Specification<T>[] subspecifications)
        {
            CompositionFunction = compositionFunction;
            Subspecifications = subspecifications;
        }

        public override bool IsSatisfiedBy(T obj) =>
            CompositionFunction(
                Subspecifications.Select(spec => spec.IsSatisfiedBy(obj)));
    }
}