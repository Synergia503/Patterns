﻿using System;

namespace AbstractFactoryDemo.Specification.Validation.Infrastructure
{
    // This is for wrapping other specifications. This is how Specification Trees can be built. 
    // This will be useful to test if the certain property of the target object is null (using inside NotNull Specification).
    internal class Property<TType, TProperty> : Specification<TType>
    {
        private Func<TType, TProperty> PropertyGetter { get; }
        private Specification<TProperty> Subspecification { get; }

        // This delegate will extract the certain property of the target object and then test it against (sub)specification.
        public Property(Func<TType, TProperty> propertyGetter, Specification<TProperty> subspecification)
        {
            PropertyGetter = propertyGetter;
            Subspecification = subspecification;
        }

        public override bool IsSatisfiedBy(TType obj) =>
            Subspecification.IsSatisfiedBy(
                PropertyGetter(obj));
    }
}