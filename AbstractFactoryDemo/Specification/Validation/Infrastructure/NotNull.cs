﻿namespace AbstractFactoryDemo.Specification.Validation.Infrastructure
{
    internal class NotNull<T> : Specification<T>
    {
        // This specification tests whether the object passed to the input is different from null.
        // Quite simple and effective way to capture one primitive question.
        public override bool IsSatisfiedBy(T obj) => obj is object;
    }
}