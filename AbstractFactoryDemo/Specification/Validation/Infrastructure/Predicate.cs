﻿using System;

namespace AbstractFactoryDemo.Specification.Validation.Infrastructure
{
    // This specification can test infinite number of predicates, no matter how complex they are.
    internal class Predicate<T> : Specification<T>
    {
        private Func<T, bool> Delegate { get; }

        public Predicate(Func<T, bool> predicate)
        {
            Delegate = predicate;
        }

        public override bool IsSatisfiedBy(T obj) =>
            Delegate(obj);

    }
}