﻿using AbstractFactoryDemo.Specification;

namespace AbstractFactoryDemo
{
    public static class BuildingSpecificationExtensions
    {
        public static bool SafeEquals<T>(this IBuildingSpecification<T> first, IBuildingSpecification<T> second)
        {
            bool firstNull = first is null;
            bool secondNull = second is null;

            return (firstNull && secondNull) || (!firstNull && first.Equals(second));
        }
    }
}