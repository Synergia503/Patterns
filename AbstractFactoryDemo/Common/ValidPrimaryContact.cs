﻿using AbstractFactoryDemo.Interfaces;
using System;

namespace AbstractFactoryDemo.Common
{
    public class ValidPrimaryContact : IPrimaryContactState
    {
        private IContactInfo Contact { get; }
        private Func<IContactInfo, bool> Predicate { get; }

        public ValidPrimaryContact(IContactInfo contact, Func<IContactInfo, bool> predicate)
        {
            if (contact is null || predicate is null)
            {
                throw new ArgumentNullException();
            }

            if (!predicate(contact))
            {
                throw new ArgumentException();
            }

            Contact = contact;
            Predicate = predicate;
        }

        public IContactInfo Get()
            => Contact;

        public IPrimaryContactState Set(IContactInfo contact)
            => new ValidPrimaryContact(contact, Predicate);
    }
}