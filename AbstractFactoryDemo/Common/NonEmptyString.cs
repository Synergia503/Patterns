﻿using System;

namespace AbstractFactoryDemo.Common
{
    public class NonEmptyString : INonEmptyStringState
    {
        // It is good idea to make State object immutable - for example by private field.
        private string Value { get; set; }

        public NonEmptyString(string value)
        {
            // Value is only set in the constructor and it will never be changed during the lifetime of the object.
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException();
            }

            Value = value;
        }

        public string Get()
            => Value;

        // An advice: All preconditions should be explicit.
        // But then -> Set() method is not entitled to add preconditions - it will violate Liskov SP. This Set() is violating it anyway - here implicitly through inaction.
        // To not to get into this trap again (to bypass it) Interface contracts should be created -> preconditions defined at the abstract interface.
        public INonEmptyStringState Set(string value)
            => new NonEmptyString(value); // -> Problem - Set() method has an implicit precondition! It is not throwing an exception but it is invoking the constructor which will throw an exception.
                                          // And this is not a good idea to rely on others (objects, methods) when it comes to throwing exceptions. Set() method should have an explicit precodonditions~!
    }
}