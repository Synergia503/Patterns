﻿using AbstractFactoryDemo.Interfaces;

namespace AbstractFactoryDemo.Common
{
    public interface IPrimaryContactState
    {
        IPrimaryContactState Set(IContactInfo contact);
        IContactInfo Get();
    }
}