﻿using AbstractFactoryDemo.Interfaces;
using System;

namespace AbstractFactoryDemo.Common
{
    public class UninitializedPrimaryContact : IPrimaryContactState
    {
        private Func<IContactInfo, bool> Predicate { get; }

        public UninitializedPrimaryContact(Func<IContactInfo, bool> predicate)
        {
            Predicate = predicate ?? throw new ArgumentNullException();
        }

        public IContactInfo Get()
            => throw new InvalidOperationException();

        public IPrimaryContactState Set(IContactInfo contact)
        {
            if (contact is null)
            {
                throw new ArgumentNullException();
            }

            return new ValidPrimaryContact(contact, Predicate);
        }
    }
}