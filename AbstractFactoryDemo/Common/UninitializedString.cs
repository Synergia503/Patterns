﻿using System;

namespace AbstractFactoryDemo.Common
{
    public class UninitializedString : INonEmptyStringState
    {
        // Remember -> NonEmptyString constructor comes with an implicit precondition - here we are violating LSP once again.
        public INonEmptyStringState Set(string value)
            => new NonEmptyString(value);

        public string Get()
           => throw new InvalidOperationException();
    }
}