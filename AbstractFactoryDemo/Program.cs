﻿using AbstractFactoryDemo.Buillders;
using AbstractFactoryDemo.Factories.Interfaces;
using AbstractFactoryDemo.FactoryMethod;
using AbstractFactoryDemo.Models;
using AbstractFactoryDemo.Specification;
using System;

namespace AbstractFactoryDemo
{
    // ************************* Noble goal *************************

    // Code that will fail at runtime should fail to compile.
    // Alas this is not alwayas reachable.

    // ************************* Noble goal *************************

    class Program
    {
        static void Main(string[] args)
        {
            //ConfigureUser();
            NotifyUser();
            ConfigureMachine();

            SpecificationPattern.Sample();
        }

        private static void NotifyUser()
        {
            //PersonalManager personalManager = new PersonalManager(ConstructPerson); // or
            EmailAddress contact = new EmailAddress("max.planck@new-mit.com");
            PersonalManager personalManager = new PersonalManager(
                PersonBuilder
                .Person()
                .WithFirstName("Max")
                .WithLastName("Planck")
                .WithPrimaryContact(contact)
                .WithAdditionalContact(new EmailAddress("max.pl@cosmo.net"))
                .WithAdditionalContact(new EmailAddress("maxoplanc@net.net"))
                .AndNoMoreContacts()
                .Build); // Build() method is not invoked here, it is passed as delegate, because Build() method is the Factory Method here. 
            personalManager.Notify("yes");
        }

        static void RegisterUser(IUserFactory userFactory)
        {
            //ITicketHolder user = userFactory.CreateUser("", "");
        }

        static void ConfigureUser()
        {
            //PersonBuilder personBuilder = new PersonBuilder();
            //personBuilder.SetFirstName("Max");
            //personBuilder.SetLastName("Planck");
            //EmailAddress contact = new EmailAddress("max.planck@new-mit.com");
            //personBuilder.AddContact(contact);
            //personBuilder.AddContact(new EmailAddress("another@mit.com"));
            //personBuilder.SetPrimaryContact(contact); // with this line commented code will break with an exception!
            // Builder pattern does not cover the process of building the product entirely! It only covers all the details of its product. It means that client is free to mix steps in any way it likes.
            // sometimes this would be fine, for instance setting primary contacts before adding second one or before setting last and first name. But there are some order of steps which have to be preserved (which can be violated by accident!).
            // This is the Builder responsibility to bend and adapt to the rules of its product. Lesson learned = building process comes with certain Calling Protocol. But public interface does not communicate this protocol at all
            // Builder pattern left the doors open for errors made by the client. 
            // Builder pattern should have the particular order of called method.
            // For instance - SetFirstName should be forced to be called before Build() method. Client should not be able even to see Build() method if First Name was forgotten to be set.
            // This is Interface Segregation Principle.
            // Person person = personBuilder.Build();

            // Constructing product with obeyed ISP:
            Person person = ConstructPerson();

            Console.WriteLine(person);
        }

        private static void ConfigureMachine()
        {
            // Machine Builder works different than PersonBuilder although they are building same abstract IUser object. But concrete object differs very much thus there is a need to introduce two separate builders
            // Sequence of calls that constructs valid Machine object is different than for constructing valid Person object. That is the consequence of Builder Pattern. 
            // With Factory Method pattern we can hide concrete product.
            // This delegate comes with same signature, but its body is completely different!
            //Func<IUserFactoryMethod> machineFactory = MachineBuilder
            //    .Machine()
            //    .WithProducer(new Producer())
            //    .WithModel("some model")
            //    .OwnedBy(new LegalEntity("some company", new EmailAddress("some-company@co.net"), new PhoneNumber(48, 9, 1232123123)))
            //    .Build;

            //PersonalManager personalManager = new PersonalManager(machineFactory);
            PersonalManager personalManager = new PersonalManager(UserFactories.MachineFactory);
            personalManager.Notify("hello");
        }

        private static Person ConstructPerson()
        {
            EmailAddress contact = new EmailAddress("max.planck@new-mit.com");

            Person person = PersonBuilder //new PersonBuilder()// public constructor here is very bad idea, cause client can access all public methods for instance Build() method, even in the beginning of building process.
                .Person()
                .WithFirstName("Max")
                .WithLastName("Planck")
                .WithPrimaryContact(contact)
                .WithAdditionalContact(new EmailAddress("max.pl@cosmo.net"))
                .WithAdditionalContact(new EmailAddress("maxoplanc@net.net"))
                .AndNoMoreContacts()
                .Build();
            return person;
        }
    }
}