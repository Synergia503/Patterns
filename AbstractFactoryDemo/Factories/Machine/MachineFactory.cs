﻿using AbstractFactoryDemo.Factories.Interfaces;
using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Models;
using System;
using System.Collections.Generic;

namespace AbstractFactoryDemo.Factories.Machine
{
    public class MachineFactory : IUserFactory
    {
        private Dictionary<string, Producer> NameToProducer { get; }

        public MachineFactory(Dictionary<string, Producer> nameToProducer)
        {
            NameToProducer = nameToProducer ?? throw new ArgumentNullException(nameof(nameToProducer));
        }

        public IUserIdentity CreateIdentity()
        {
            throw new NotImplementedException();
        }

        private Producer GetProducer(string name)
        {
            if (!NameToProducer.TryGetValue(name, out Producer producer))
                throw new ArgumentException();

            return producer;
        }

        public IUser CreateUser(string name1, string name2)
        {
            Producer producer = GetProducer(name1);
            LegalEntity owner =
                new LegalEntity("Big Co.", new EmailAddress("big@co"), new PhoneNumber(1, 2, 3));

            return new Models.Machine(producer, name2, owner);
        }
    }
}