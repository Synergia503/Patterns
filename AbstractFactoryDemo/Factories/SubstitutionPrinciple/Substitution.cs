﻿using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Models;

namespace AbstractFactoryDemo.Factories.SubstitutionPrinciple
{
    public class Substitution
    {
        public void HowDoesItWork()
        {
            // Substitution principle - we can assign more derived type to the less derived type .
            // variable assignment is covariant by its nature
            // But passing arguments is contravariant by its nature.
            // However it should be used rather for generics types.
            // Generic type T is less derived than itself. // awkward but it is so.
            IUserIdentity userIdentity = new IdentityCard();
        }
    }
}