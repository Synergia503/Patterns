﻿namespace AbstractFactoryDemo.DesignByContract
{
    class DbC
    {
        // Bertrand Meyer, Object-Oriented Software Construction

        // Samples:
        // if - then - throw pattern (precondition)
        // preconditions must not be strengthen in derived types! Otherwise, Substitution Principle won't hold.

        // Pluralsight courses:
        // Provable Code by Michael Perry
        // Code Contracts by John Sonmez - it's about Code Contract library
    }
}