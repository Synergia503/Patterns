﻿using AbstractFactoryDemo.Buillders.Interfaces;
using AbstractFactoryDemo.Interfaces;
using System.Collections.Generic;

namespace AbstractFactoryDemo.Buillders
{
    // Builder design pattern is not only for making it possible for the client to build the complex object but also to navigate the client through save object construction process and give well built product.
    // An idea - manage builder's state with State pattern
    // Builder should work as a Finit State Machine - it should manage its internal state and all internal actions are governed by its state.
    // Each call to the State object must return new state from its Set() method and current State has to be replaced.    
    // After applying CQS part, there are preconditions missing - in course library CodeContract was introduced but in .NET Core it does not exist anymore.
    // Preconditions should thus not be moved away.
    public class PersonBuilder : IFirstNameHolder, ILastNameHolder, IPrimaryContactHolder, IContactHolder, IPersonBuilder
    {
        private PersonBuilder()
        {
            Contacts = new List<IContactInfo>();
            // PrimaryContactState = new UninitializedPrimaryContact(ContainsContact);
        }

        public static IFirstNameHolder Person()
            => new PersonBuilder();

        // private Func<string> GetValidFirstName { get; set; }
        //    = () => { throw new InvalidOperationException(); };

        // With ISP there is no need to have State Pattern, Builder turned into Finit State Machine
        // State Pattern served us good, but design without it will be even better.
        // Explicit States are not of little value, they can be good solution in many cases.
        // private INonEmptyStringState FirstNameState { get; set; } = new UninitializedString();

        // private Func<string> GetValidLastName { get; set; }
        //    = () => { throw new InvalidOperationException(); };

        // With ISP there is no need to have State Pattern, Builder turned into Finit State Machine
        // State Pattern served us good, but design without it will be even better.
        // Explicit States are not of little value, they can be good solution in many cases.
        // private INonEmptyStringState LastNameState { get; set; } = new UninitializedString();

        private string FirstName { get; set; }

        private string LastName { get; set; }

        private IContactInfo PrimaryContact { get; set; }

        // With ISP there is no need to have State Pattern, Builder turned into Finit State Machine
        // State Pattern served us good, but design without it will be even better.
        // Explicit States are not of little value, they can be good solution in many cases.
        //private IPrimaryContactState PrimaryContactState { get; set; }

        private IList<IContactInfo> Contacts { get; set; }

        private bool ContainsContact(IContactInfo contact) => Contacts.Contains(contact);


        // LSP violation:
        // WithFirstName() implements method from IFirstNameHolder interface.
        // Interface did not declare that any exception can be thrown. This violates LSP. Any client accustomed to use this interface would never expect to see thrown exception from this method.
        // But this concrete implementation is adding precondition and client will be surprised that it will one day throw an exception.
        // The root cause of this problem is that Implementation is adding precondition. This should not be allowed (according to design by contract practise) when overriding methods in the derived class. 
        // This was not obeying CQS Principle
        //public ILastNameHolder WithFirstName(string firstName)
        //{
        //    if (string.IsNullOrEmpty(firstName))
        //        throw new ArgumentException(nameof(firstName));
        //    FirstName = firstName;
        //    //GetValidFirstName = () => firstName;
        //    //FirstNameState = FirstNameState.Set(firstName);

        //    return this;
        //}

        // This obeys CQS Principle
        public ILastNameHolder WithFirstName(string firstName)
            => new PersonBuilder()
            {
                FirstName = firstName
            };


        // This was not obeying CQS Principle
        //public IPrimaryContactHolder WithLastName(string lastName)
        //{
        //    if (string.IsNullOrEmpty(lastName))
        //        throw new ArgumentException(nameof(lastName));
        //    LastName = lastName;
        //    //GetValidLastName = () => lastName;
        //    //LastNameState = LastNameState.Set(lastName);
        //    return this;
        //}


        // This obeys CQS Principle
        public IPrimaryContactHolder WithLastName(string lastName) =>
          new PersonBuilder()
          {
              FirstName = FirstName,
              LastName = lastName
          };

        // This was not obeying CQS Principle
        //public IContactHolder WithAdditionalContact(IContactInfo contact)
        //{
        //    if (contact is null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    if (Contacts.Contains(contact))
        //    {
        //        throw new ArgumentException();
        //    }

        //    Contacts.Add(contact);

        //    return this;
        //}


        // This was not obeying CQS Principle
        //public IContactHolder WithPrimaryContact(IContactInfo primaryContact)
        //{
        //    if (primaryContact is null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    WithAdditionalContact(primaryContact);
        //    //PrimaryContactState = PrimaryContactState.Set(contact);
        //    PrimaryContact = primaryContact;
        //    return this;
        //}


        // This obeys CQS Principle and ISP Principle. And with this shape it guarantees that WithPrimaryContact() method will be called exactly and only once!


        // ******************************************** Design guideline ********************************************
        // Combine interfaces with immutability! To incorporate certain guarantees in the design. Client of this class won't make mistake because it's impossible to make them, when class interface is designed this way.
        public IContactHolder WithPrimaryContact(IContactInfo contact) =>
         new PersonBuilder()
         {
             FirstName = FirstName,
             LastName = LastName,
             Contacts = new List<IContactInfo>(Contacts) { contact },
             PrimaryContact = contact
         };


        // This obeys CQS Principle
        public IContactHolder WithAdditionalContact(IContactInfo contact) =>
            new PersonBuilder()
            {
                FirstName = FirstName,
                LastName = LastName,
                Contacts = new List<IContactInfo>(Contacts) { contact },
                PrimaryContact = PrimaryContact
            };

        public IPersonBuilder AndNoMoreContacts()
        {
            return this;
        }

        // An idea - make builder unable to call the constructor without ensuring internal state's correctness.
        // Builder should be unable to break the Person object.
        // There are 2 problems current:
        // 1# Builder is not controlling the process of building its product - it cannot say anything about things being passed to that Builder - only throwing an exception is the way to get out (here is not implemented, but validation for First and LastName would look like this - throwing exception)
        // 2# Imperative data validation - this should not be part of Object Oriented code.
        public Models.Person Build()
        {
            //var person = new Models.Person(FirstNameState.Get(), LastNameState.Get());
            var person = new Models.Person // (FirstName, LastName);
            {
                Name = FirstName,
                LastName = LastName
            };

            foreach (IContactInfo contact in Contacts)
            {
                person.ContactsList.Add(contact);
            }

            //person.SetPrimaryContact(PrimaryContactState.Get());
            //person.SetPrimaryContact(PrimaryContact);
            person.PrimaryContact = PrimaryContact;
            return person;
            //return new Models.Person(FirstName, LastName);
        }
    }
}