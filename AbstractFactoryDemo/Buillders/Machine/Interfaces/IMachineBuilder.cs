﻿using AbstractFactoryDemo.Models;

namespace AbstractFactoryDemo.Buillders.Machine.Interfaces
{
    public interface IMachineBuilder
    {
        Models.Machine Build();
    }

    public interface IModelHolder
    {
        IOwned WithModel(string model);
    }

    public interface IOwned
    {
        IMachineBuilder OwnedBy(LegalEntity company);
    }

    public interface IProducerHolder
    {
        IModelHolder WithProducer(Producer producer);
    }
}