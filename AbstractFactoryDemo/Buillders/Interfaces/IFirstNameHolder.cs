﻿using AbstractFactoryDemo.Interfaces;

namespace AbstractFactoryDemo.Buillders.Interfaces
{
    public interface IFirstNameHolder
    {
        ILastNameHolder WithFirstName(string firstName);
    }

    public interface ILastNameHolder
    {
        IPrimaryContactHolder WithLastName(string lastName);
    }

    public interface IPrimaryContactHolder
    {
        IContactHolder WithPrimaryContact(IContactInfo contact);
    }

    public interface IContactHolder
    {
        IContactHolder WithAdditionalContact(IContactInfo contact);
        IPersonBuilder AndNoMoreContacts();
    }

    public interface IPersonBuilder
    {
        Models.Person Build();
    }
}