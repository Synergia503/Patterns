﻿namespace AbstractFactoryDemo.HowCreatingObjectWorks
{
    class InMemory
    {
        class Base
        {
            private int fieldA;
            private byte fieldB;
            private int fieldC;

            // When we create an object its fields are located in subsequent memory locations.
            // Reference to an object means holding address to a memory location of the first byte of the first field of this object.
            // fieldA = fieldB + fieldC -> in memory:
            // executable code: [base + 0B] <- [base + 4B] + [base + 5B]
            // when a method is compiled every access to the fields is transformed into an offset
            // of that field from the beginning of the object - number of bytes that we have to move from
            // the first byte of the object to reach the first of the field we want to access 
            // what does it mean 'current object'?
            // every instance level method comes with implicit 'this' reference
            // Purpose - address the first byte of the object located in memory.
            // Whenever some method in invoked 'this' is implicitly passed.
        }

        class Derived : Base
        {
            // this field will be placed in memory just after fields defined in the base class
            // this class can come with new methods and/or with new fields and/or fields changing fields in the base class.
            // this changes will not affect object in memory
            // Only calculations for the new fields will be new - arithmetic for the old fields remains ([base + 0B].....)
            // todo make notes about virtual tables and type descriptors
            private int fieldX;
        }
    }
}