﻿using AbstractFactoryDemo.Factories.Interfaces;
using AbstractFactoryDemo.Factories.Person;
using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Models;

namespace AbstractFactoryDemo.HowCreatingObjectWorks
{
    // Substitution Principle is about syntatical correctness
    // LSP is about semantical correcntess

    public class LiskovSubstitutionPrinciple

    // Rules:
    // 1. No preconditions in method overrides - this is dangerous for polymorphics calls (you can easily make inherited methods fail). But constructor has nothing to do with LSP. Constructor in not an instance level method of the object
    // 2. Constructor does not belong to the object, it belongs to the class. It can never appear in the virtual functions table. Derivced class cannot override the base class constructor.
    // 3. Thus, there is no such thing as constructor overriding, even when derived type comes with constructor with same set of arguments as the one in the base class. Overriding requires a function address which is located in virtual functions table. 
    // 4. When we add constructor for derived class the compiler jumps in and embeds a call to the base class constructor as the first action in the derived class constructor body. That is something which does not apply to virtual methods.
    // 5. Thus, constructors are not polymorphic. This could be bad news, cause all fuss with abstract factory is to provide something like 'polymorphic constructor'.
    // 6. We are eligible to add as many precondition checks to derived constructor as we want without affecting LSP and causing any inadvertent effects on objects substitutions in general.
    // Advice: Have constructor preconditions added to every class - using an object means that in was constructed correctly.

    {
        public void ShowViolation()
        {
            IUserFactory userFactory = new PersonFactory();
            IUser user = userFactory.CreateUser("", "");

            IUserIdentity userIdentity = new MacAddress(); // userFactory.CreateIdentity();

            // with userFactory.CreateIdentity() everything works correctly because method underneath knows how to use IdentityCard object, which comes from Personfactory
            // with MacAddress SetUserIdentity will unexpectedly (for client) fail.
            // But from the Substitution Principle point of view that kind of replacement is possible and legal.
            // Cause MacAddress is type of IUserIdentity, compiler has no troubles building this code, but an exception will be thrown.
            // Substitution Principle alone is not sufficient to guarantee correctness of the code.
            // SP speaks of subtyping while the problem does not come from type, but from behaviour of the object.
            // This kind of the situation is described by the behavioural subtyping.

            // !!!!!!!!!!!!!!!!! We have to be sure that the object on the right side obeys same behaviour as base object on the left side.
            // There is no need to preserve implementation, there is need to REFLECT the same behaviour.

            // Behaviour vs implementation
            // more general motion - what the function is doing vs. how it is doing (what means are performed to do the task)
            // Implementation makes behaviour happen in one way or the other.
            // Changing implemenatation does not change behaviour - it should stay that way. 

            // object on the right side should preserving ALL behaviours inherited from its base type.
            // throwing an exception in concrete implementation of SetUserIdentity means altering behaviour.
            // It is possible to invoke method and end up with an object which does not have an Identity set. 
            // because implementation decided to throw an exception instead of setting Identity object which it was supposed to do.



            // What is really important - exceptions ARE chaning behaviour of the methods/classes.


            // LSP
            // Do substitution only if behaviour didn't change 
            // and changing the actual object will not endanger the client (cause client relies on behaviour)
            // and application execution will remain the same

            // Substituting an object with an object of a more derived type does not affect code correctness.
            // We must create/design classes that break changes are not introduced in derived types. 

            // if clause and throwing exception is method precondition. This does not exist in base class.

            // Adding preconditions in derived class is bad idea - subclass should be able to handle all cases that its base could handle.
            // or even more cases, but NONO to reduce capabilities of base class.

            user.SetUserIdentity(userIdentity);
        }

        // This approach is one of the most bug-ridden method of creating objects.
        // This is one of the most prominent sources of defects. 
        // One entity creates an object and passes it to another, which creates one more object and puts this into interaction. 
        public void Violation2()
        {
            ConfigureUser(new IdentityCard());
        }

        private void ConfigureUser(IUserIdentity outerId)
        {
            // Creating object (user) in this situation should not be delegated to somewhere else 
            // (for instance we should not pass IUserIdentity as argument), because we won't have idea what runtime type will be passed.

            IUserFactory factory = new PersonFactory();
            IUser user = factory.CreateUser("", "");

            // Seems like a good idea, but this is copied pasted logic from SetUserIdentity
            // the problem here is, that we assume, that concrete object is Person object - but how can we even assume that?
            // There is a coupling between CreateUser method and this dynamic type checking. 
            // This code is easy to break if we change the type of factory. That will cause the runtime type User to change as well
            // and the type checking logic below will come inappropriate.

            //if (outerId is IdentityCard)
            //{
            //    user.SetUserIdentity(outerId);
            //}
            //else
            //{
            //    System.Console.WriteLine("Not an identity card.");
            //}



            // this type checking can/should be delegegated to Abstract User - IUser - boolean method
            // It may looks like an obtrusive way, but IUser is base type and now it can tell that concrete type will/won't fail. 
            // in terms of preconditions we move precondition check to abstract interface
            // this solution strictly obeys LSP.
            // all preconditions have been defined on the abstract type, concrete derived type only adds implementation. 
            // On the client side this can looks awful and complicated, but this is most general case we have here.
            // The object, which might cause the failure has been passed from the outside, from the place wihtout our control. 
            // we could rely only on SetUserIdentity in case, when outerId would not exists, meaning it wouldn't be passed,
            // so that this line will be OK user.SetUserIdentity(outerId);
            if (user.CanAcceptIdentity(outerId))
            {
                // this is correct, because client now can decide what to do, but it doesn't have its own logic, it only uses implementation from concrete type
                // client knows the context, why this CanAcceptIdentity should be called. And only this caller is the only one who can decide what to do with result.
                user.SetUserIdentity(outerId);
                //IUserIdentity userIdentity = factory.CreateIdentity();
            }

            // of course this could be hidden behind the interface, to ensure that it is not going to be changed in any subsequent redesign.
            // this is risky part, and thus can/should be closed in class, class which will be the builder
            // in that way we will ensure that between  "IUserIdentity userIdentity = factory.CreateIdentity();" and "user.SetUserIdentity(outerId);"
            // no code will be ever placed (because it should not, if it would be placed it could cause precondition violation) .
            //IUserFactory factory = new PersonFactory();
            //IUser user = factory.CreateUser("", "");
            //IUserIdentity userIdentity = factory.CreateIdentity();
            //user.SetUserIdentity(outerId);

        }
    }
}