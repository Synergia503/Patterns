﻿namespace AbstractFactoryDemo.Interfaces
{
    // This interface was introduced only for presentation purposes.
    // In reality there would be only IUser interface.
    public interface IUserFactoryMethod : IUser
    {
        IContactInfo PrimaryContact { get; }
        //void SetIdentity(IUserIdentity identity);
        //bool CanAcceptIdentity(IUserIdentity identity);
        //void Add(IContactInfo contact);
        //void SetPrimaryContact(IContactInfo contact);
    }
}