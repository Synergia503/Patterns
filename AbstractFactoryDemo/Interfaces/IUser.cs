﻿namespace AbstractFactoryDemo.Interfaces
{
    //public interface ITicketHolder
    //{

    //}

    public interface IUser //: ITicketHolder 
    {
        void SetUserIdentity(IUserIdentity userIdentity);
        bool CanAcceptIdentity(IUserIdentity userIdentity);
        //void AddContact(IContactInfo contact);
        //void SetPrimaryContact(IContactInfo contact);
    }
}