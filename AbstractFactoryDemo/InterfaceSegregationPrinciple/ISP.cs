﻿namespace AbstractFactoryDemo.InterfaceSegregationPrinciple
{
    public class ISP
    {
        // ISP starts with an observation that when the class exposes to many public methods to the client, this client depends on methods it does not want to call (or at least does not want to call immediately).
        // This is the problem because developer who writes the code can mistake methods and cause runtime failure.
        // ISP says - this large public interface can be cut into segments, and inside a segment members must be higly related, there must be a high cohesion.
        // In the Builder pattern there should be an enforcement to call methods from separate interfaces in predictable (by designer) manner.
        // ISP means that operations offered by a class can be segregated into several groups, each group holding closly related features.
    }
}