﻿using AbstractFactoryDemo.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace AbstractFactoryDemo.Models
{
    public class LegalEntity
    {
        public string CompanyName { get; internal set; }
        public EmailAddress EmailAddress { get; internal set; }
        public PhoneNumber PhoneNumber { get; internal set; }
        public IEnumerable<IContactInfo> OtherContacts { get; internal set; }

        internal LegalEntity() { }

        public LegalEntity(string companyName, EmailAddress emailAddress, PhoneNumber phoneNumber)
        {
            CompanyName = companyName;
            EmailAddress = emailAddress;
            PhoneNumber = phoneNumber;
        }

        public void SetEmailAddress(EmailAddress emailAddress)
        {
            EmailAddress = emailAddress;
        }

        public void SetPhoneNumber(PhoneNumber phoneNumber)
        {
            PhoneNumber = phoneNumber;
        }

        public override string ToString() =>
           $"{CompanyName} {EmailAddress} {PhoneNumber} [{OtherContactsToString()}]";

        private string OtherContactsToString() =>
            string.Join(", ", OtherContactsToStringArray());

        private string[] OtherContactsToStringArray() =>
            OtherContacts.Select(contact => contact.ToString()).ToArray();

        //public void Add(IContactInfo alternateContact)
        //{
        //    OtherContacts.Add(alternateContact);
        //}

        //public bool Contains(IContactInfo contact)
        //{
        //    return
        //        EmailAddress.Equals(contact) ||
        //        PhoneNumber.Equals(contact) ||
        //        OtherContacts.Contains(contact);
        //}
    }
}