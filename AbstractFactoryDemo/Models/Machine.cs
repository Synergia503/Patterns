﻿using AbstractFactoryDemo.Interfaces;

namespace AbstractFactoryDemo.Models
{
    public class Machine : IUser, IUserFactoryMethod
    {
        public Producer Producer { get; internal set; }
        public string Model { get; internal set; }
        public LegalEntity Owner { get; internal set; }

        public IContactInfo PrimaryContact => Owner.EmailAddress;

        internal Machine() { }

        public Machine(Producer producer, string model, LegalEntity owningBusiness)
        {
            Producer = producer;
            Model = model;
            Owner = owningBusiness;
        }

        public void SetUserIdentity(IUserIdentity identity)
        {
        }

        public bool CanAcceptIdentity(IUserIdentity identity)
        {
            return identity is MacAddress;
        }

        public override string ToString() =>
           $"{Producer} {Model} owned by {Owner}";

        //public void SetPrimaryContact(IContactInfo contact)
        //{   // NOTE: It would be better to throw if contact is not email address
        //    // but that requires a new Boolean method like IsValidPrimaryContact
        //    // so that code contract can be implemented properly

        //    EmailAddress emailAddress = contact as EmailAddress;
        //    if (emailAddress == null)
        //        Owner.Add(contact);
        //    else
        //        Owner.SetEmailAddress(emailAddress);
        //}

        //public void Add(IContactInfo contact)
        //{
        //    Owner.Add(contact);
        //}
    }
}