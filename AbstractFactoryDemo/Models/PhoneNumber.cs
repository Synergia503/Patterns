﻿using AbstractFactoryDemo.Interfaces;

namespace AbstractFactoryDemo.Models
{
    public class PhoneNumber : IContactInfo
    {
        public int CountryCode { get; internal set; }
        public int AreaCode { get; internal set; }
        public int Number { get; internal set; }

        internal PhoneNumber() { }

        public PhoneNumber(int countryCode, int areaCode, int number)
        {
            CountryCode = countryCode;
            AreaCode = areaCode;
            Number = number;
        }

        public override bool Equals(object obj)
        {
            PhoneNumber other = obj as PhoneNumber;
            return
                other != null && other.CountryCode == CountryCode &&
                other.AreaCode == AreaCode && other.Number == Number;
        }

        public override string ToString() => $"+{CountryCode}({AreaCode}){Number}";
    }
}