﻿using AbstractFactoryDemo.Interfaces;
using System;

namespace AbstractFactoryDemo.Models
{
    public class EmailAddress : IContactInfo
    {
        public string Address { get; internal set; }

        internal EmailAddress() { }

        public EmailAddress(string address)
        {
            Address = address ?? throw new ArgumentNullException();
        }

        public override int GetHashCode()
        {
            return Address.ToLower().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EmailAddress email))
                return false;

            return string.Compare(Address, email.Address, StringComparison.InvariantCultureIgnoreCase) == 0;
        }

        public override string ToString() => $"{Address}";
    }
}