﻿using AbstractFactoryDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbstractFactoryDemo.Models
{
    public class Person : IUser, IUserFactoryMethod
    {
        // Person class turned into simple data object holding data, without checking all those preconditions and without building logic.
        // Data validation should never be found in data classes, cause this data validation/building process is infrastructure around this Person object which is product. And this infrastructure was meant to be outside of this Person class.
        // Now, Person class can focus on its behaviour, not on its construction.
        internal Person()//(string name, string surname)
        {
            // Construction logic should be moved away!
            //if (string.IsNullOrEmpty(name))
            //    throw new ArgumentException("First name must be non-empty.");

            //if (string.IsNullOrEmpty(surname))
            //    throw new ArgumentException("Last name must be non-empty.");

            //Name = name;
            //Surname = surname;
        }

        public IEnumerable<IContactInfo> Contacts => ContactsList;
        public IContactInfo PrimaryContact { get; internal set; }

        internal IList<IContactInfo> ContactsList { get; set; } = new List<IContactInfo>();

        public string Name { get; set; }
        public string LastName { get; set; }

        // This is implementation not behaviour , so throwing exception is not allowed here and should be added to base type.
        // This also ensures that dynamic type checking is added into only one place.
        public bool CanAcceptIdentity(IUserIdentity userIdentity)
            => userIdentity is IdentityCard;

        public void SetUserIdentity(IUserIdentity userIdentity)
        {
            //// contravariance example
            //IdentityCard identityCard = userIdentity as IdentityCard;

            // this is much cleaner and much safer to the client.
            // type checking is only in one place, and all classes implementing IUser will have the same behaviour of type checking.
            // client does have to know implementation details of type checking.
            if (!CanAcceptIdentity(userIdentity))
            {
                throw new Exception();
            }

            // old, bad sample
            //if (identityCard == null)
            //{
            //    throw new Exception();
            //}

            IdentityCard identityCard = userIdentity as IdentityCard;
            // do something with identityCard.SSN;

            //userIdentity.SSN = ""; // Problem with Abstract Factory - they are abstract, cannot set here some details.
            // There are no excursions to concrete area, they are 100 % abstract.
        }

        public override string ToString() =>
           $"{Name} {LastName} [{AllContactsLabel}]";

        private string AllContactsLabel =>
          string.Join(", ", AllContactLabels.ToArray());

        private IEnumerable<string> AllContactLabels =>
            Contacts.Select(GetLabelFor);

        private string GetLabelFor(IContactInfo contact) =>
            $"{GetUiMarkFor(contact)}{contact}";

        private string GetUiMarkFor(IContactInfo contact) =>
            IsPrimary(contact) ? "*" : string.Empty;

        private bool IsPrimary(IContactInfo contact) => contact.Equals(PrimaryContact);


        // Construction logic should be moved away!
        //public void AddContact(IContactInfo contact)
        //{
        //    if (contact is null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    if (ContactsList.Contains(contact))
        //    {
        //        throw new ArgumentException();
        //    }

        //    ContactsList.Add(contact);
        //}

        // Construction logic should be moved away!
        //public void SetPrimaryContact(IContactInfo contact)
        //{
        //    if (contact is null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    if (!ContactsList.Contains(contact))
        //    {
        //        throw new ArgumentException();
        //    }

        //    ContactsList.Add(contact);
        //}
    }
}