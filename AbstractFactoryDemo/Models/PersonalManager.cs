﻿using AbstractFactoryDemo.Interfaces;
using System;

namespace AbstractFactoryDemo.Models
{
    public class PersonalManager
    {
        // The one who is constructing the PersonalManager object is now responsible for providing valid method of creating Person object.
        public PersonalManager(Func<IUserFactoryMethod> createUserFactoryMethod)
        {
            UserFactory = createUserFactoryMethod;
        }

        public void Notify(string message)
        {
            Enqueue(GetPrimaryContact(), message);
        }

        // This class does not know how the user will be created - this is a good thing having on mind all those complicated details that Person creation process involves.
        // Factory Method Design Pattern simplifies the class in which the Factory Method is declared. It makes the class focuses on its primary responsibility. 
        // Creating an object is recognized as added responsibility and it is deferred until derived class is implemented.
        //protected abstract IUserFactoryMethod CreateUser();

        // Other way of creating user - by delegate. CreateUser() few lines below means calling/invoking delegate, and this call will return an object IUserFactoryMethod IUser interface
        //protected abstract Func<IUserFactoryMethod> CreateUser { get; }

        private Func<IUserFactoryMethod> UserFactory { get; }

        private IContactInfo GetPrimaryContact()
        {
            return UserFactory().PrimaryContact;
        }

        private void Enqueue(IContactInfo contact, string message)
        {
            System.Console.WriteLine($"Sending '{message}' to '{contact}'.");
        }
    }

    // This class is not needed anymore, cause the way for creating Person object is now achieved by injecting factory in the PersonalManager's constructor.
    //public class HumanPersonalManager : PersonalManager
    //{
    //    // In reality we don't need that derived class, this method, this lambda can be injected into PersonalManager class like in Strategy Design Pattern. 
    //    protected override Func<IUserFactoryMethod> CreateUser
    //    {
    //        // This property getter is not supposed to return an object anymore. It should return a delegate. This is lambda syntax in C#.
    //        // This property getter returns a delegate to function receiving no arguments and returns an Person object
    //        get
    //        {
    //            EmailAddress contact = new EmailAddress("max.planck@new-mit.com");
    //            return () => PersonBuilder
    //                .Person()
    //                .WithFirstName("Max")
    //                .WithLastName("Planck")
    //                .WithPrimaryContact(contact)
    //                .WithAdditionalContact(new EmailAddress("max.pl@cosmo.net"))
    //                .WithAdditionalContact(new EmailAddress("maxoplanc@net.net"))
    //                .AndNoMoreContacts()
    //                .Build();
    //        }
    //    }



    // Methods like that deserve their own classes just in sake of code clarity. And they have big task - take an entity from for example database and create it - this should not be handled in another class.
    // This concrete logic of creating concrete product just does not fit in base class, because it handles general concepts.
    // But creating class only for getting and creating user can be too much, there are other possibilities.
    //protected override IUserFactoryMethod CreateUser()
    //{
    //    // This is for presentation purposes, this would be rather pull out from database for instance
    //    EmailAddress contact = new EmailAddress("max.planck@new-mit.com");
    //    return PersonBuilder
    //        .Person()
    //        .WithFirstName("Max")
    //        .WithLastName("Planck")
    //        .WithPrimaryContact(contact)
    //        .WithAdditionalContact(new EmailAddress("max.pl@cosmo.net"))
    //        .WithAdditionalContact(new EmailAddress("maxoplanc@net.net"))
    //        .AndNoMoreContacts()
    //        .Build();
    //}
    //}
}