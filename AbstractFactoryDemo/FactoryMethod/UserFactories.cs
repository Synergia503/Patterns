﻿using AbstractFactoryDemo.Buillders;
using AbstractFactoryDemo.Buillders.Machine;
using AbstractFactoryDemo.Interfaces;
using AbstractFactoryDemo.Models;
using System;

namespace AbstractFactoryDemo.FactoryMethod
{
    // Higher order function concept is shown here.
    // This is convienient, because all the mess with functions here will be called only if PersonalManager succeeds to call Notify method, so only then it will instantiate all the objects and methods below.
    // These functions make no changes in the system until the system is in need of their result.
    // 
    public static class UserFactories
    {
        public static Func<IUserFactoryMethod> PersonFactory =>
            PersonBuilder
                .Person()
                .WithFirstName("Max")
                .WithLastName("Planck")
                .WithPrimaryContact(new EmailAddress("max.planck@my-institute.com"))
                .WithAdditionalContact(new EmailAddress("max@home-of-plancks.com"))
                .AndNoMoreContacts()
                .Build;

        public static Func<IUserFactoryMethod> MachineFactory => CreateMachineFactory(CreateLegalEntity);

        private static Func<IUserFactoryMethod> CreateMachineFactory(Func<LegalEntity> ownerFactory) =>
            MachineBuilder
                .Machine()
                .WithProducer(new Producer())
                .WithModel("fast-one")
                .OwnedBy(ownerFactory())
                .Build;

        // Before refactoring: 
        // and call: CreateMachineFactory(CreateLegalEntity()) -> But this differs from origin implementation:
        //private static Func<IUserFactoryMethod> CreateMachineFactory() =>
        // MachineBuilder
        //     .Machine()
        //     .WithProducer(new Producer())
        //     .WithModel("fast-one")
        //     .OwnedBy(new LegalEntity(.......))
        //     .Build;
        // because in origin implementation we are building LegalEntity only when MachineBuilder was set properly and after two methods
        // but below we get the LegalEntity already built . This is not good refactoring, in big projects we could break the code!
        // Refactoring process should not cause any changes in the execution process.
        private static Func<IUserFactoryMethod> CreateMachineFactory(LegalEntity legalEntity) =>
          MachineBuilder
              .Machine()
              .WithProducer(new Producer())
              .WithModel("fast-one")
              .OwnedBy(legalEntity)
              .Build;

        private static Func<LegalEntity> CreateLegalEntityFactory(Func<EmailAddress> emailAddressFactory)
            => () => new LegalEntity("Big Co.", emailAddressFactory(),
                       new PhoneNumber(123, 45, 6789));

        private static Func<LegalEntity> CreateLegalEntity =>
            CreateLegalEntityFactory(() => new EmailAddress("big@co"));
    }
}