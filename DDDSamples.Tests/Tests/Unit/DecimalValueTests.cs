﻿using DDDSamples.Presentation.Domain.Infrastructure;
using FluentAssertions;
using System;
using Xunit;

namespace DDDSamples.Presentation.Domain.Apartments.Tests.Unit
{
    public class DecimalValueTests
    {
        [Theory]
        [InlineData(1.11d, 1, 11)]
        [InlineData(223.12321d, 223, 12321)]
        public void should_retrieve_before_decimal_part_and_decimal_part_when_correct_decimal_value_provided(double number, uint expectedBeforeDecimalPart, uint expectedDecimalPart)
        {
            // arrange & act
            DecimalValue decimalValue = new DecimalValue(number);

            // assert
            decimalValue.ValueBeforeSeparator.Should().Be(expectedBeforeDecimalPart);
            decimalValue.ValueAfterSeparator.Should().Be(expectedDecimalPart);
        }

        [Theory]
        [InlineData(1234)]
        [InlineData(0001)]
        [InlineData(1111)]
        [InlineData(1111.111)]
        [InlineData(999.00001)]
        [InlineData(1232.090908)]
        [InlineData(1232090908)]
        [InlineData(1.232090908)]
        [InlineData(1.0000000000000001)]
        [InlineData(0999991.000000000000000)]
        [InlineData(0000000000000.0000000000999)]
        public void should_be_equal_when_two_same_values_provided_first_constructor(double value)
        {
            DecimalValue decimalSeparator = new DecimalValue(value);
            DecimalValue decimalSeparator2 = new DecimalValue(value);
            //DecimalValue decimalSeparatorFromTwoNumbers = new DecimalValue(9, 99);
            //DecimalValue decimalSeparatorFromTwoNumbers2 = new DecimalValue(9, 99);

            decimalSeparator.Should().Be(decimalSeparator2);
            decimalSeparator.GetHashCode().Should().Be(decimalSeparator2.GetHashCode());
        }

        [Theory]
        [InlineData(00000000000001.9000000000999)]
        [InlineData(00000000000009.9000000000999)]
        [InlineData(90000000000000.9000000000999)]
        [InlineData(90000000000000.000000000999)]
        public void should_throw_when_too_big_or_too_low_value_provided_first_constructor(double value)
        {
            Action shouldThrow = () => new DecimalValue(value);

            shouldThrow.Should().ThrowExactly<DomainException>().WithMessage($"Given number is too big or too small: '{value}'.");
        }

        [Theory]
        [InlineData(-11.1232)]
        [InlineData(-31123.000000123)]
        [InlineData(-0.123298)]
        [InlineData(-0.0000000009)]
        public void should_throw_exception_when_negative_number_given(double value)
        {
            Action shouldThrow = () => new DecimalValue(value);

            shouldThrow.Should().ThrowExactly<DomainException>();
        }
    }
}