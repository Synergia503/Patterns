﻿namespace Demo2.Start
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}