﻿namespace Demo2.Completed
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}