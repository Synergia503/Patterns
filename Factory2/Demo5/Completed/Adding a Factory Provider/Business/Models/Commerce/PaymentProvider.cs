﻿namespace Demo5.Completed
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}