﻿namespace Demo5.Completed
{
    public interface IInvoice
    {
        public byte[] GenerateInvoice();
    }
}
