﻿namespace Demo5.Start
{
    public interface ISummary
    {
        string CreateOrderSummary(Order order);

        void Send();
    }
}
