﻿namespace Demo5.Start
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}