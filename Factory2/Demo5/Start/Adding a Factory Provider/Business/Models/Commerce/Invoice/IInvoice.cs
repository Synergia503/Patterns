﻿namespace Demo5.Start
{
    public interface IInvoice
    {
        public byte[] GenerateInvoice();
    }
}
