﻿namespace Demo3.Completed
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}