﻿namespace Demo3.Completed
{
    public interface ISummary
    {
        string CreateOrderSummary(Order order);

        void Send();
    }
}
