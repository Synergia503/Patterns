﻿namespace Demo3.Completed
{
    public interface IInvoice
    {
        public byte[] GenerateInvoice();
    }
}
