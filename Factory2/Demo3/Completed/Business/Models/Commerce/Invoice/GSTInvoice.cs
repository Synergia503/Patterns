﻿using System.Text;

namespace Demo3.Completed
{
    public class GSTInvoice : IInvoice
    {
        public byte[] GenerateInvoice()
        {
            return 
                Encoding
                .Default
                .GetBytes("Hello world from a GST Invoice");
        }
    }
}
