﻿namespace Demo3.Completed
{
    public interface IPurchaseProviderFactory
    {
        // It is no required in Abstract Factory Pattern, that all methods gets the same parameter. 
        // The methods can also return any kind of type which is an abstract class (or its child) or an interface
        ShippingProvider CreateShippingProvider(Order order);
        IInvoice CreateInvoice(Order order);
        ISummary CreateSummary(Order order);
    }
}
