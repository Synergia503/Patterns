﻿namespace Demo3.Start
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}