﻿namespace Demo1.Completed
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}