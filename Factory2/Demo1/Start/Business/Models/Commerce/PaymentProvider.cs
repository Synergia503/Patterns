﻿namespace Demo1.Start
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}