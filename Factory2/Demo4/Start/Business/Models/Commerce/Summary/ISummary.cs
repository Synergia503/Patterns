﻿namespace Demo4.Start
{
    public interface ISummary
    {
        string CreateOrderSummary(Order order);

        void Send();
    }
}
