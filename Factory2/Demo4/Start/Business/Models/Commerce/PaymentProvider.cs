﻿namespace Demo4.Start
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}