﻿namespace Demo4.Start
{
    public interface IInvoice
    {
        public byte[] GenerateInvoice();
    }
}
