﻿namespace Demo4.Completed
{
    public interface ISummary
    {
        string CreateOrderSummary(Order order);

        void Send();
    }
}
