﻿namespace Demo4.Completed
{
    public enum PaymentProvider
    {
        Paypal,
        CreditCard,
        Invoice
    }
}