﻿namespace Demo4.Completed
{
    public interface IInvoice
    {
        public byte[] GenerateInvoice();
    }
}
