﻿using Factory.Additions.Cheese;
using Factory.Additions.Dough;
using Factory.Additions.Pepperoni;
using Factory.Additions.Sauce;
using Factory.Additions.SeeFood;
using Factory.Additions.Vegetable;

namespace Factory.Factories
{
    public class AmericanAdditions : IPizzaAdditionsFactory
    {
        public ICheese CreateCheese()
        {
            return new Mozarella();
        }

        public IDough CreateDough()
        {
            return new Additions.Dough.Big();
        }

        public IPepperoni CreatePepperoni()
        {
            return new Medium();
        }

        public ISauce CreateSauce()
        {
            return new Tomato();
        }

        public ISeeFood CreateSeeFood()
        {
            return new Frozen();
        }

        public IVegetable[] CreateVegetables()
        {
            return new IVegetable[] { new BlackOlives(), new Spinacchi(), new Aubergine() };
        }
    }
}
