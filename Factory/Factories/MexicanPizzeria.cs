﻿using System;

namespace Factory.Factories
{
    public class MexicanPizzeria : Pizzeria
    {
        public override Pizza CreatePizza(string pizzaType)
        {
            if (pizzaType.Equals("Cheese"))
            {
                Console.WriteLine("Mexican cheese");
            }
            else if (pizzaType.Equals("Vegetarian"))
            {
                Console.WriteLine("Mexican vegetarian");
            }
            else if (pizzaType.Equals("Hawaii"))
            {
                Console.WriteLine("Mexican hawaii");
            }
            else
            {
                Console.WriteLine("Mexican ordinary");
            }

            return null;
        }
    }
}
