﻿using Factory.Additions.Cheese;
using Factory.Additions.Dough;
using Factory.Additions.Pepperoni;
using Factory.Additions.Sauce;
using Factory.Additions.SeeFood;
using Factory.Additions.Vegetable;

namespace Factory.Factories
{
    public class ItalianAdditions : IPizzaAdditionsFactory
    {
        public ICheese CreateCheese()
        {
            return new Reggiano();
        }

        public IDough CreateDough()
        {
            return new Thin();
        }

        public IPepperoni CreatePepperoni()
        {
            return new Medium();
        }

        public ISauce CreateSauce()
        {
            return new Marinara();
        }

        public ISeeFood CreateSeeFood()
        {
            return new Fresh();
        }

        public IVegetable[] CreateVegetables()
        {
            return new IVegetable[] { new Garlic(), new Onion(), new Champignons(), new RedPepper() };
        }
    }
}