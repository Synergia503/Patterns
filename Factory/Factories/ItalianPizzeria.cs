﻿using Factory.Pizzas;
using System;

namespace Factory.Factories
{
    public class ItalianPizzeria : Pizzeria
    {
        public override Pizza CreatePizza(string pizzaType)
        {
            Pizza pizza = null;
            var italianAdditionsFactory = new ItalianAdditions();

            if (pizzaType.Equals("Cheese"))
            {
                pizza = new CheesePizza(italianAdditionsFactory)
                {
                    Name = "Italian cheese pizza"
                };
            }
            else if (pizzaType.Equals("Vegetarian"))
            {
                Console.WriteLine("Italian vegetarian");
            }
            else if (pizzaType.Equals("Hawaii"))
            {
                Console.WriteLine("Italian hawaii");
            }
            else
            {
                Console.WriteLine("Italian another pizza");
            }

            return pizza;
        }
    }
}

