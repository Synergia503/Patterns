﻿using Factory.Pizzas;
using System;

namespace Factory.Factories
{
    public class AmericanPizzeria : Pizzeria
    {
        public override Pizza CreatePizza(string pizzaType)
        {
            Pizza pizza = null;
            var americanAdditionsFactory = new AmericanAdditions();

            if (pizzaType.Equals("Cheese"))
            {
                pizza = new CheesePizza(americanAdditionsFactory)
                {
                    Name = "American cheese pizza"
                };
            }
            else if (pizzaType.Equals("Vegetarian"))
            {
                Console.WriteLine("American vegetarian");
            }
            else if (pizzaType.Equals("Hawaii"))
            {
                Console.WriteLine("American hawaii");
            }
            else
            {
                Console.WriteLine("Another american pizza");
            }

            return pizza;
        }
    }
}
