﻿using Factory.Additions.Cheese;
using Factory.Additions.Dough;
using Factory.Additions.Pepperoni;
using Factory.Additions.Sauce;
using Factory.Additions.SeeFood;
using Factory.Additions.Vegetable;

namespace Factory
{
    public interface IPizzaAdditionsFactory
    {
        IDough CreateDough();
        ISauce CreateSauce();
        ICheese CreateCheese();
        IVegetable[] CreateVegetables();
        IPepperoni CreatePepperoni();
        ISeeFood CreateSeeFood();
    }
}