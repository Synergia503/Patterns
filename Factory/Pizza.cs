﻿using Factory.Additions.Cheese;
using Factory.Additions.Dough;
using Factory.Additions.Pepperoni;
using Factory.Additions.Sauce;
using Factory.Additions.SeeFood;
using Factory.Additions.Vegetable;
using System;
using System.Collections.Generic;

namespace Factory
{
    public abstract class Pizza
    {
        public string Name { get; set; }
        public IDough Dough { get; set; }
        public ISauce Sauce { get; set; }
        public List<IVegetable> Vegetables { get; set; } = new List<IVegetable>();
        public ICheese Cheese { get; set; }
        public IPepperoni Pepperoni { get; set; }
        public ISeeFood SeeFood { get; set; }

        public abstract void Prepare();

        public void Pack()
        {
            Console.WriteLine("Pack");
        }

        public virtual void Cut()
        {
            Console.WriteLine("Cut");
        }

        public void Bake()
        {
            Console.WriteLine("Bake");
        }
    }
}