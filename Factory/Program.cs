﻿using Factory.Factories;
using System;

namespace Factory
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizzeria pizzeria = new AmericanPizzeria();
            Pizzeria pizzeria2 = new ItalianPizzeria();
            var pizza = pizzeria.OrderPizza("Cheese");
            Console.WriteLine($"First: {pizza.Name}");

            pizza = pizzeria2.OrderPizza("Cheese");
            Console.WriteLine($"Second: {pizza.Name}");
        }
    }
}