﻿namespace Factory
{
    public abstract class Pizzeria
    {
        public Pizza OrderPizza(string pizzaType)
        {
            Pizza pizza = CreatePizza(pizzaType);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Pack();

            return pizza;
        }

        public abstract Pizza CreatePizza(string pizzaType);
    }
}