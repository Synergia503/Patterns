﻿namespace Factory.Additions.Vegetable
{
    public class Garlic : IVegetable
    {
    }

    public class Onion : IVegetable
    {
    }

    public class Aubergine : IVegetable
    {
    }

    public class Spinacchi : IVegetable
    {
    }

    public class BlackOlives : IVegetable
    {
    }

    public class RedPepper : IVegetable
    {
    }

    public class Champignons : IVegetable
    {
    }

    public interface IVegetable
    {
    }
}