﻿namespace Factory.Additions.Dough
{
    public class Thin : IDough
    {
    }

    public class Big : IDough
    {
    }

    public interface IDough
    {
    }
}