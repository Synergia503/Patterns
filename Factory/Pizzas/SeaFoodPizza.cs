﻿using System;

namespace Factory.Pizzas
{
    public class SeaFoodPizza : Pizza
    {
        private readonly IPizzaAdditionsFactory _pizzaAdditionsFactory;

        public SeaFoodPizza(IPizzaAdditionsFactory pizzaAdditionsFactory)
        {
            _pizzaAdditionsFactory = pizzaAdditionsFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine($"Preparing: {Name}");
            Dough = _pizzaAdditionsFactory.CreateDough();
            Cheese = _pizzaAdditionsFactory.CreateCheese();
            Sauce = _pizzaAdditionsFactory.CreateSauce();
            SeeFood = _pizzaAdditionsFactory.CreateSeeFood();
        }
    }
}