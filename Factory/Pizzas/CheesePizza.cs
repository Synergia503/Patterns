﻿using System;

namespace Factory.Pizzas
{
    public class CheesePizza : Pizza
    {
        private readonly IPizzaAdditionsFactory _pizzaAdditionsFactory;

        public CheesePizza(IPizzaAdditionsFactory pizzaAdditionsFactory)
        {
            _pizzaAdditionsFactory = pizzaAdditionsFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine($"Preparing: {Name}");
            Dough = _pizzaAdditionsFactory.CreateDough();
            Sauce = _pizzaAdditionsFactory.CreateSauce();
            Cheese = _pizzaAdditionsFactory.CreateCheese();
        }
    }
}