﻿using System;

namespace DDDSamples
{
    //todo: replace with Attribute
    public class SomethingChanged : IDomainEvent
    {

    }

    class SomethingChangedHandler : IHandler<SomethingChanged>
    {
        public void Handle(SomethingChanged domainEvent)
        {
            Console.WriteLine("SomethingChanged");
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class EventAttribute : Attribute { }
}