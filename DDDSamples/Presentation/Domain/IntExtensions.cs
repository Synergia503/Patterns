﻿namespace DDDSamples.Presentation.Domain
{
    public static class IntExtensions
    {
        public static bool IsPositive(this int value)
        {
            if (value > 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsNegative(this int value)
        {
            if (value < 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsPositiveOrZero(this int value)
        {
            if (value >= 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsNegativeOrZero(this int value)
        {
            if (value <= 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsZero(this int value)
        {
            if (value == 0)
            {
                return true;
            }

            return false;
        }
    }
}