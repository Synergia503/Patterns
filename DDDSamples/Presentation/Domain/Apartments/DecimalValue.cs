﻿using DDDSamples.Presentation.Domain.Infrastructure;
using System;
using System.Collections.Generic;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class DecimalValue : ValueObject
    {
        public uint ValueBeforeSeparator { get; private set; }
        public uint ValueAfterSeparator { get; private set; }

        public DecimalValue(int valueBeforeSeparator, int valueAfterSeparator)
        {
            ValidateValuesAfterSeparatingThem(valueBeforeSeparator, valueAfterSeparator);
        }

        public DecimalValue(double value)
        {
            decimal decimalValue = ((decimal)value % 1);
            int decimalValueAsUnitValue = GetDecimalPartAsUnitValueOrThrow(value, decimalValue);
            int beforeDecimalValue = GetUnitValueOrThrow(value, decimalValue);
            ValidateValuesAfterSeparatingThem(beforeDecimalValue, decimalValueAsUnitValue);
        }

        private int GetUnitValueOrThrow(double value, decimal decimalValue)
        {
            try
            {
                return (int)((decimal)value - decimalValue);
            }
            catch (OverflowException ex)
            {
                throw new DomainException($"Given number is too big or too small: '{value}'.", ex);
            }
        }

        private int GetDecimalPartAsUnitValueOrThrow(double value, decimal decimalValue)
        {
            try
            {
                int decimalNumberCount = BitConverter.GetBytes(decimal.GetBits((decimal)(double)value)[3])[2];
                return (int)(decimalValue * (decimal)Math.Pow(10, decimalNumberCount));
            }
            catch (OverflowException ex)
            {
                throw new DomainException($"Given number is too big or too small: '{value}'.", ex);
            }
        }

        private void ValidateValuesAfterSeparatingThem(int valueBeforeSeparator, int valueAfterSeparator)
        {
            if (valueBeforeSeparator.IsNegative())
            {
                throw new DomainException($"Bad value before decimal part: '{valueBeforeSeparator}'.");
            }

            if (valueAfterSeparator.IsNegative())
            {
                throw new DomainException($"Bad decimal value part: '{valueAfterSeparator}'.");
            }

            if (valueBeforeSeparator.IsZero() && valueAfterSeparator.IsZero())
            {
                throw new DomainException($"Bad value: '{valueBeforeSeparator},{valueAfterSeparator}'.");
            }

            ValueBeforeSeparator = (uint)valueBeforeSeparator;
            ValueAfterSeparator = (uint)valueAfterSeparator;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return ValueAfterSeparator;
            yield return ValueBeforeSeparator;
        }

        public static implicit operator DecimalValue(double value)
           => new DecimalValue(value);
    }
}