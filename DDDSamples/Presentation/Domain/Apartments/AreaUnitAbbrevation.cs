﻿using DDDSamples.Presentation.Domain.Infrastructure;
using System.Collections.Generic;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class AreaUnitAbbrevation : ValueObject
    {
        private static readonly List<string> _supportedUnitAbbrevations = new List<string>() { "m2", "cm2" };

        public static readonly AreaUnitAbbrevation SquareMeter = new AreaUnitAbbrevation("m2");
        public static readonly AreaUnitAbbrevation SquareCentimeter = new AreaUnitAbbrevation("cm2");

        public bool IsSquareMeter() => Value == "m2";
        public bool IsSquareCentimeter() => Value == "cm2";

        public string Value { get; private set; }

        public AreaUnitAbbrevation(string abbrevation)
        {
            ValidateAreaUnitAbbrevation(abbrevation);
        }

        private void ValidateAreaUnitAbbrevation(string abbrevation)
        {
            if (string.IsNullOrWhiteSpace(abbrevation))
            {
                throw new DomainException("Area unit abbrevation must not be empty.");
            }

            if (!_supportedUnitAbbrevations.Contains(abbrevation.ToLowerInvariant()))
            {
                throw new DomainException($"Unsupported area unit abbrevation: '{abbrevation}'.");
            }

            Value = abbrevation;
        }


        public static implicit operator string(AreaUnitAbbrevation abbrevation)
            => abbrevation.Value;

        public static implicit operator AreaUnitAbbrevation(string abbrevation)
            => new AreaUnitAbbrevation(abbrevation);

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value.ToLowerInvariant();
        }
    }
}