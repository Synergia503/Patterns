﻿using DDDSamples.Presentation.Domain.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class Area : ValueObject
    {
        private static readonly IReadOnlyList<AreaUnit> _supportedUnits =
            new List<AreaUnit>() { AreaUnit.SquareCentimeter, AreaUnit.SquareMeter };

        public AreaUnit Unit { get; private set; }

        public DecimalValue AreaValue { get; }

        public DecimalSeparator DecimalSeparator { get; }

        public Area(AreaUnit unit, double areaValue, char decimalSeparator)
        {
            ValidateAreaUnit(unit);
            AreaValue = areaValue;
            DecimalSeparator = decimalSeparator;
        }

        private void ValidateAreaUnit(AreaUnit unit)
        {
            if (!_supportedUnits.Contains(unit))
            {
                throw new DomainException($"Bad unit '{unit}'.");
            }

            Unit = unit;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Unit;
            yield return DecimalSeparator;
            yield return AreaValue;
        }
    }
}