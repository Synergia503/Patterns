﻿namespace DDDSamples.Presentation.Domain.Apartments
{
    public class RoomInApartment : Entity
    {
        public Apartment Apartment { get; }
        public Room Room { get; }

        public RoomInApartment(Apartment apartment, Room room)
        {
            Apartment = apartment;
            Room = room;
        }
    }
}