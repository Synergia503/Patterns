﻿using DDDSamples.Presentation.Domain.Infrastructure;
using System.Collections.Generic;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class AreaUnitFullName : ValueObject
    {
        private readonly List<string> _supportedUnitFullNames = new List<string>() { "square meter", "square centimeter" };

        public static readonly AreaUnitFullName SquareMeter = new AreaUnitFullName("m2");
        public static readonly AreaUnitFullName SquareCentimeter = new AreaUnitFullName("cm2");

        public bool IsSquareMeter() => Value == "square meter";
        public bool IsSquareCentimeter() => Value == "square centimeter";

        public string Value { get; private set; }

        public AreaUnitFullName(string fullName)
        {
            ValidateAreaUnitAbbrevation(fullName);
        }

        private void ValidateAreaUnitAbbrevation(string fullName)
        {
            if (string.IsNullOrWhiteSpace(fullName))
            {
                throw new DomainException("Area unit full name must not be empty.");
            }

            if (!_supportedUnitFullNames.Contains(fullName.ToLowerInvariant()))
            {
                throw new DomainException($"Unsupported area unit full name: '{fullName}'.");
            }

            Value = fullName;
        }


        public static implicit operator string(AreaUnitFullName fullName)
            => fullName.Value;

        public static implicit operator AreaUnitFullName(string fullName)
            => new AreaUnitFullName(fullName);

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value.ToLowerInvariant();
        }
    }
}