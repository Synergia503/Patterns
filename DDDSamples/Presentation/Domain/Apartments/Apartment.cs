﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class Apartment : AggregateRoot
    {
        private readonly IList<RoomInApartment> _rooms;

        public IReadOnlyList<Room> Rooms =>
            _rooms
            .Select(ria => ria.Room)
            .ToList();

        public Area Space { get; }

        public void AddRoom(Room room)
        {
            _rooms.Add(new RoomInApartment(this, room));
        }
    }
}