﻿using DDDSamples.Presentation.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class DecimalSeparator : ValueObject
    {
        private readonly IReadOnlyList<char> _allowedSeparators = new List<char>() { '.', ',' };

        public char Value { get; private set; }

        public DecimalSeparator(char separator)
        {
            ValidateSeparator(separator);
        }

        private void ValidateSeparator(char separator)
        {
            if (!_allowedSeparators.Contains(separator))
            {
                throw new DomainException($"Unsupported separator: '{separator}'.");
            }

            Value = separator;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }

        public static implicit operator char(DecimalSeparator separator)
           => separator.Value;

        public static implicit operator DecimalSeparator(char separator)
            => new DecimalSeparator(separator);
    }
}