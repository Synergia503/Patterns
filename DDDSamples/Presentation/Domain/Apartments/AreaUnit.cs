﻿using DDDSamples.Presentation.Domain.Infrastructure;
using System.Collections.Generic;

namespace DDDSamples.Presentation.Domain.Apartments
{
    public class AreaUnit : ValueObject
    {
        public static readonly AreaUnit SquareMeter
            = new AreaUnit(AreaUnitAbbrevation.SquareMeter, AreaUnitFullName.SquareMeter);

        public static readonly AreaUnit SquareCentimeter
            = new AreaUnit(AreaUnitAbbrevation.SquareCentimeter, AreaUnitFullName.SquareCentimeter);

        public AreaUnitAbbrevation Abbreviation { get; private set; }
        public AreaUnitFullName FullName { get; private set; }

        public AreaUnit(AreaUnitAbbrevation abbreviation, AreaUnitFullName fullName)
        {
            Validate(abbreviation, fullName);
            Abbreviation = abbreviation;
            FullName = fullName;
        }

        public AreaUnit(string abbreviation, string fullName)
        {
            Validate(abbreviation, fullName);
        }

        private void Validate(AreaUnitAbbrevation abbreviation, AreaUnitFullName fullName)
        {
            if (AreUnitCandidatesContrary(abbreviation, fullName))
            {
                throw new DomainException(
                    $"Unit candidates are contrary. Abbrevation candidate: '{abbreviation}'. Fullname candidate: '{fullName}'.");
            }

            Abbreviation = abbreviation;
            FullName = fullName;

            static bool AreUnitCandidatesContrary(AreaUnitAbbrevation abbreviation, AreaUnitFullName fullName)
            {
                return
                    (abbreviation.IsSquareMeter() && fullName.IsSquareCentimeter())
                       ||
                    (abbreviation.IsSquareCentimeter() && fullName.IsSquareMeter());
            }
        }

        public static implicit operator string(AreaUnit areaUnit)
            => areaUnit.ToString();

        public override string ToString()
            => $"{FullName} - {Abbreviation}";

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return FullName.Value.ToLowerInvariant();
            yield return Abbreviation.Value.ToLowerInvariant();
        }
    }
}