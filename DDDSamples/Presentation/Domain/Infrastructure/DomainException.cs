﻿using System;

namespace DDDSamples.Presentation.Domain.Infrastructure
{
    public class DomainException : Exception
    {
        public string ErrorCode { get; }

        public DomainException(string message) : this(message, errorCode: null)
        {
        }

        public DomainException(string message, string errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }

        public DomainException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}