﻿namespace DDDSamples
{
    //https://docs.microsoft.com/en-us/archive/blogs/ericlippert/curiouser-and-curiouser
    abstract class Animal<T> where T : Animal<T>
    {
        public abstract void MakeFriends(T animal);
    }

    class Cat : Animal<Cat>
    {
        public override void MakeFriends(Cat cat) { }
    }

    class Dog : Animal<Dog>
    {
        public override void MakeFriends(Dog cat) { }
    }

    class EvilDog : Animal<Cat>
    {
        public override void MakeFriends(Cat animal) { }
    }
}