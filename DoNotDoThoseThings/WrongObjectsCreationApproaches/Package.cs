﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DoNotDoThoseThings.WrongObjectsCreationApproaches
{
    //todo: write unit tests
    public class Package
    {
        // Anemic classes (only properties, default parameterless constructors) are bad
        // because client can create object without setting properties. 
        // Then, if some another code uses those properties nasty bugs can (or rather will) come up
        // Beside that, what should for example Package object mean without basic properties like
        // Width, Height, Length? It's pointless to instantiate that object without assigning values to those properties.
        // Client should not be afraid of creating object. Client should be sure, that when it wants create object, that object will be created without failure.

        private double length;

        public double Width { get; set; }
        public double Height { get; set; }

        public double Length
        {
            get => length;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Lenght must not be below or equal zero.", nameof(Length));
                }

                length = value;
            }
        }
    }

    public class PackageManager
    {
        public decimal CalculateValueOfAllPackages(IEnumerable<Package> packages)
        {
            decimal value = packages.Sum(p => CalculateSinglePackagePrice(p));
            return value;
        }

        private decimal CalculateSinglePackagePrice(Package package)
        {
            return (decimal)(package.Height * package.Length * package.Width / 100 * 0.4);
        }
    }

    public class CostCalculator
    {
        public decimal CalculateCost()
        {
            var package = new Package();
            var packages = new List<Package>
            {
                new Package(),
                package,
                new Package { Length = 1, Height = 2, Width = 3 }
            };

            var packageManager = new PackageManager();
            decimal value = packageManager.CalculateValueOfAllPackages(packages);
            return value;
        }
    }
}