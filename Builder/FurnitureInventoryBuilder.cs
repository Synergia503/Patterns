﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Builder
{
    public class FurnitureInventoryBuilder : IFurnitureInventoryBuilder
    {
        private InventoryReport _report;
        private readonly IEnumerable<FurnitureItem> _items;

        public FurnitureInventoryBuilder(IEnumerable<FurnitureItem> items)
        {
            Reset();
            _items = items;
        }

        public void Reset()
        {
            _report = new InventoryReport();
        }

        public IFurnitureInventoryBuilder AddDimensions()
        {
            _report.DimensionSection = $"Dimension section: {string.Join(' ', _items.Select(x => x.Height))}";
            return this;
        }

        public IFurnitureInventoryBuilder AddLogistics(DateTime generatedOn)
        {
            _report.LogisticsSection = $"Generated on: {generatedOn}";
            return this;
        }

        public IFurnitureInventoryBuilder AddTitle()
        {
            _report.TitleSection = "Title";
            return this;
        }

        public InventoryReport GetDailyReport()
        {
            InventoryReport finishedReport = _report;
            Reset();

            return finishedReport;
        }
    }

    public interface IFurnitureInventoryBuilder
    {
        IFurnitureInventoryBuilder AddTitle(); // Could be void. With returning interface it's Fluent Builder
        IFurnitureInventoryBuilder AddDimensions(); // Could be void. With returning interface it's Fluent Builder
        IFurnitureInventoryBuilder AddLogistics(DateTime generatedOn); // Could be void. With returning interface it's Fluent Builder
        InventoryReport GetDailyReport();
    }

    public class ReportBuildDirector
    {
        private readonly IFurnitureInventoryBuilder _builder;

        public ReportBuildDirector(IFurnitureInventoryBuilder builder)
        {
            _builder = builder;
        }

        public void BuildCompleteReport()
        {
            _builder
                .AddTitle()
                .AddDimensions()
                .AddLogistics(DateTime.Now);
        }
    }
}