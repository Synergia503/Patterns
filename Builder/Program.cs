﻿using System;
using System.Collections.Generic;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            var items = new List<FurnitureItem>
            {
                new FurnitureItem("Chair",9.99,10,100,50),
                new FurnitureItem("Cupboard",100.99,100,200,550),
                new FurnitureItem("TAble",889.99,550,800,350)
            };

            var builder = new FurnitureInventoryBuilder(items);
            // Without fluent builder:
            //var director = new ReportBuildDirector(builder);
            //director.BuildCompleteReport();
            //var dailyReport = builder.GetDailyReport();

            var dailyReport = builder
               .AddTitle()
               .AddDimensions()
               .AddLogistics(DateTime.Now)
               .GetDailyReport();

            Console.WriteLine(dailyReport.Debug());
        }
    }
}