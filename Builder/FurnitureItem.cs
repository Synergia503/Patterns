﻿namespace Builder
{
    public class FurnitureItem
    {
        public FurnitureItem(string name, double price, double weight, double width, double height)
        {
            Name = name;
            Price = price;
            Weight = weight;
            Width = width;
            Height = height;
        }

        public string Name;
        public double Price;
        public double Weight;
        public double Width;
        public double Height;
    }
}