﻿using System.Text;

namespace Builder
{
    public class InventoryReport
    {
        public string TitleSection;
        public string DimensionSection;
        public string LogisticsSection;

        public string Debug()
        {
            return new StringBuilder()
                .AppendLine(TitleSection)
                .AppendLine(DimensionSection)
                .AppendLine(LogisticsSection)
                .ToString();
        }
    }
}