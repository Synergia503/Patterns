﻿using Decorator.Cafe;
using Decorator.ComputerShop;
using Decorator.InputStream;

namespace Decorator
{
    static class Program
    {
        static void Main(string[] args)
        {
            //var runner = new CafeRunner();
            //runner.Run();
            //var streamRunner = new InputStreamRunner();
            //streamRunner.Run();
            ComputerRunner.Run();
        }
    }
}