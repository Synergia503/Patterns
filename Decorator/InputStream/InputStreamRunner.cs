﻿using System;
using System.IO;

namespace Decorator.InputStream
{
    public class InputStreamRunner
    {
        public void Run()
        {
            int @char;
            Stream lowerCaseStream = new LowerCaseInputStream(File.OpenRead("LowerCaseInputFile.txt"));
            while ((@char = lowerCaseStream.ReadByte()) >= 0)
            {
                Console.Write((char)@char);
            }

            Console.WriteLine();
        }
    }
}