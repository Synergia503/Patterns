﻿using Decorator.ComputerShop.Domain;

namespace Decorator.ComputerShop.Decorators
{
    // Not the best pattern, presentations purposes only, each class should be in separate file

    public class Memory8Option : ComputerDecorator
    {
        public Memory8Option(ComputerBase computerBase) : base(computerBase)
        {
            OptionCode = "8RAM";
            Price = 70.0M;
        }
    }

    public class Memory16Option : ComputerDecorator
    {
        public Memory16Option(ComputerBase computerBase) : base(computerBase)
        {
            OptionCode = "16RAM";
            Price = 150.0M;
        }
    }

    public class SSD128Option : ComputerDecorator
    {
        public SSD128Option(ComputerBase computerBase) : base(computerBase)
        {
            OptionCode = "128SSD";
            Price = 74.0M;
        }
    }

    public class SSD256Option : ComputerDecorator
    {
        public SSD256Option(ComputerBase computerBase) : base(computerBase)
        {
            OptionCode = "256SSD";
            Price = 120.0M;
        }
    }

    public class SSD512Option : ComputerDecorator
    {
        public SSD512Option(ComputerBase computerBase) : base(computerBase)
        {
            OptionCode = "512SSD";
            Price = 223.0M;
        }
    }
}