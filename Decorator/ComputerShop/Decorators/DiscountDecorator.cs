﻿using Decorator.ComputerShop.Domain;

namespace Decorator.ComputerShop.Decorators
{
    public class DiscountDecorator : ComputerDecorator
    {
        private readonly int _discountRate = 5;

        public DiscountDecorator(ComputerBase computerBase) : base(computerBase)
        {
            OptionCode = "Discount";
            Price = 0;
        }

        public override string GetOptionCode()
        {
            return $"{base.GetOptionCode()}, Discount: {_discountRate}";
        }

        public override decimal GetPrice()
        {
            return (100 - _discountRate) * BaseComputer.GetPrice() / 100;
        }
    }
}