﻿using Decorator.ComputerShop.Domain;

namespace Decorator.ComputerShop.Decorators
{
    public abstract class ComputerDecorator : ComputerBase
    {
        protected ComputerBase BaseComputer = null;

        protected string OptionCode = "";
        protected decimal Price = 0.0M;

        protected ComputerDecorator(ComputerBase computerBase)
        {
            BaseComputer = computerBase;
            Model = computerBase.Model;
        }

        public override string GetOptionCode()
        {
            return $"{BaseComputer.GetOptionCode()}, {OptionCode}";
        }

        public override decimal GetPrice()
        {
            return Price + BaseComputer.GetPrice();
        }
    }
}