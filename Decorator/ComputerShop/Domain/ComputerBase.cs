﻿namespace Decorator.ComputerShop.Domain
{
    public abstract class ComputerBase
    {
        public string Model { get; set; }
        public string Description { get; set; }
        public abstract string GetOptionCode();
        public abstract decimal GetPrice();
    }
}