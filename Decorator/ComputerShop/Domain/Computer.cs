﻿namespace Decorator.ComputerShop.Domain
{
    public class Computer : ComputerBase
    {
        public override string GetOptionCode()
        {
            return "";
        }

        public override decimal GetPrice()
        {
            return 499.0M;
        }
    }
}