﻿using Decorator.ComputerShop.Decorators;
using Decorator.ComputerShop.Domain;
using static System.Console;

namespace Decorator.ComputerShop
{
    public static class ComputerRunner
    {
        public static void Run()
        {
            var computer1 = new DiscountDecorator(new Memory16Option(new SSD512Option(new Computer())));
            WriteLine($"Option code: {computer1.GetOptionCode()}, price: {computer1.GetPrice()}");
        }
    }
}