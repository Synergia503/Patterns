﻿namespace Decorator.Cafe.Domain
{
    public class StarCafeSpecial : Beverage
    {
        public StarCafeSpecial()
        {
            Description = "Star Cafe Special Coffee";
        }

        public override double GetCost()
        {
            return 0.89;
        }

        public override string GetDescription()
        {
            return Description;
        }        
    }
}