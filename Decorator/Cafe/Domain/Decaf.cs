﻿namespace Decorator.Cafe.Domain
{
    public class Decaf : Beverage
    {
        public Decaf()
        {
            Description = "Decaffeinated";
        }

        public override double GetCost()
        {
            return 1.05d;
        }

        public override string GetDescription()
        {
            return Description;
        }     
    }
}