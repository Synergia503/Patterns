﻿namespace Decorator.Cafe.Domain
{
    public class DarkRoast : Beverage
    {
        public DarkRoast()
        {
            Description = "Dark Roast";
        }

        public override double GetCost()
        {
            return 0.99d;
        }

        public override string GetDescription()
        {
            return Description;
        }
    }
}