﻿namespace Decorator.Cafe.Domain
{
    public abstract class Beverage
    {
        protected string Description { get; set; } = "Unknown drink";

        protected Size Size { get; set; } = Size.Medium;

        public abstract string GetDescription();

        public abstract double GetCost();

        public void SetSize(Size size)
        {
            Size = size;
        }

        public Size GetSize()
        {
            return Size;
        }
    }

    public enum Size
    {
        Small,
        Medium,
        Large
    }
}