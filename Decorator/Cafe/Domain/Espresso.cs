﻿namespace Decorator.Cafe.Domain
{
    public class Espresso : Beverage
    {
        public Espresso()
        {
            Description = "Coffee Espresso";
        }

        public override double GetCost()
        {
            return 1.99d;
        }

        public override string GetDescription()
        {
            return Description;
        }
    }
}