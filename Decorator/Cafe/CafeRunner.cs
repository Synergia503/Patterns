﻿using Decorator.Cafe.Decorators;
using Decorator.Cafe.Domain;
using static System.Console;

namespace Decorator.Cafe
{
    public class CafeRunner
    {
        public void Run()
        {
            Beverage beverage = new Espresso();
            WriteLine($"Coffee description: {beverage.GetDescription()}, coffe price: {beverage.GetCost()}");


            Condiment beverageInline = new WhippedCream(new SoyMilk(new Chocolate(new Espresso())));
            WriteLine($"Coffee description: {beverageInline.GetDescription()}, coffe price: {beverageInline.GetCost()}");

            Beverage beverage2 = new DarkRoast();
            beverage2 = new Chocolate(beverage2);
            beverage2 = new Chocolate(beverage2);
            beverage2 = new WhippedCream(beverage2);
            WriteLine($"Coffee description: {beverage2.GetDescription()}, coffe price: {beverage2.GetCost()}");

            Beverage beverage3 = new StarCafeSpecial();
            beverage3.SetSize(Size.Small);
            beverage3 = new SoyMilk(beverage3);
            beverage3 = new Chocolate(beverage3);
            beverage3 = new WhippedCream(beverage3);
            WriteLine($"Coffee description: {beverage3.GetDescription()}, coffe price: {beverage3.GetCost()}");
        }
    }
}