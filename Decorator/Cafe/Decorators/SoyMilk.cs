﻿using Decorator.Cafe.Domain;

namespace Decorator.Cafe.Decorators
{
    public class SoyMilk : Condiment
    {
        public SoyMilk(Beverage beverage) : base(beverage)
        {
            Description = "soy milk";
        }

        public override string GetDescription()
        {
            return $"{base.GetDescription()}, {Description}";
        }

        public override double GetCost()
        {
            double cost = base.GetCost();
            Size size = GetSize();

            if (size == Size.Small)
            {
                cost += 0.1d;
            }
            else if (size == Size.Medium)
            {
                cost += 0.15d;
            }
            else if (size == Size.Large)
            {
                cost += 0.2d;
            }

            return cost;
        }
    }
}