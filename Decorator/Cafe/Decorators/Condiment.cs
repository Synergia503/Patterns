﻿using Decorator.Cafe.Domain;

namespace Decorator.Cafe.Decorators
{
    public abstract class Condiment : Beverage
    {
        protected Beverage Beverage;

        protected Condiment(Beverage beverage)
        {
            Beverage = beverage;
        }

        public override string GetDescription()
        {
            return Beverage.GetDescription();
        }

        public override double GetCost()
        {
            return Beverage.GetCost();
        }
    }
}