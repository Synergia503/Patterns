﻿using Decorator.Cafe.Domain;

namespace Decorator.Cafe.Decorators
{
    public class Chocolate : Condiment
    {
        public Chocolate(Beverage beverage) : base(beverage)
        {
            Description = "chocolate";
        }

        public override string GetDescription()
        {
            return $"{base.GetDescription()}, {Description}";
        }

        public override double GetCost()
        {
            return base.GetCost() + 0.2d;
        }
    }
}