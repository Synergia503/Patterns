﻿using Decorator.Cafe.Domain;

namespace Decorator.Cafe.Decorators
{
    public class WhippedCream : Condiment
    {
        public WhippedCream(Beverage beverage) : base(beverage)
        {
            Description = "whipped cream";
        }

        public override string GetDescription()
        {
            return $"{base.GetDescription()}, {Description}";
        }

        public override double GetCost()
        {
            return base.GetCost() + 0.1d;
        }
    }
}