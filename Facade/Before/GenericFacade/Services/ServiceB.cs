namespace Facade.Before
{
    public class ServiceB
    {
        public void Method1()
        {
            // do some work
        }

        public string Method2()
        {
            // do some work

            return "ServiceB string";
        }
    }
}