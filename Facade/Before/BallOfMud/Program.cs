﻿using System;

namespace Facade.Before
{
    class ProgramFacadeBefore
    {
        static void MainFacadeBefore(string[] args)
        {
            BigClass bigClass = new BigClass();

            bigClass.SetValueI(3);

            bigClass.IncrementI();
            bigClass.IncrementI();
            bigClass.IncrementI();

            bigClass.DecrememntI();

            Console.WriteLine($"Final Number : {bigClass.GetValueB()}");
        }
    }
}