using System;

namespace Facade.After
{
    public interface IServiceFacade
    {
        Tuple<int, double, string> CallFacade();
    }
}