﻿using System;

namespace Facade.After
{
    public static class ProgramFacadeAfter_Facade
    {
        static void MainFacadeAfter_Facade(string[] args)
        {
            const string zipCode = "98074";

            IWeatherFacade weatherFacade = new WeatherFacade();
            WeatherFacadeResults results = weatherFacade.GetTempInCity(zipCode);

            Console.WriteLine("The current temperature is {0}F/{1}C in {2}, {3}",
                                results.Fahrenheit,
                                results.Celsius,
                                results.City.Name,
                                results.State.Name);

        }
    }
}