﻿using System;

namespace Facade.After
{
    class ProgramFacadeAfter
    {
        static void MainFacadeAfter(string[] args)
        {
            BigClassFacade bigClass = new BigClassFacade();

            bigClass.IncreaseBy(50);
            bigClass.DecreaseBy(20);

            Console.WriteLine($"Final Number : {bigClass.GetCurrentValue()}");
        }
    }
}