﻿using System.Collections.Generic;
using System.Linq;

namespace PreservingEncapsulation.Module6
{
    public class Student : Entity
    {
        public string Name { get; }
        public string Email { get; }
        public virtual Course FavoriteCourse { get; }

        private readonly List<Enrollment> _enrollments = new List<Enrollment>();
        public virtual IReadOnlyList<Enrollment> Enrollments => _enrollments.ToList();
        // .ToList() prevents original collection from being changed even if the client performs a manual cast from IReadOnlyList to List
        // IEnumerable is not recommended here, the most specific type should be returned while still maintaining all the Invariants. 
        // Here the Invariant is that client code should not be able to modify the _enrollment collection, both IEnumerable and  IReadOnlyList do this task,
        // but IReadOnlyList is more specific since it inherits from IEnumerable. 
        protected Student()
        {
        }

        public Student(string name, string email, Course favoriteCourse)
            : this()
        {
            Name = name;
            Email = email;
            FavoriteCourse = favoriteCourse;
        }  

        public string EnrollIn(Course course, Grade grade)
        {
            if (_enrollments.Any(x => x.Course == course))
                return $"Already enrolled in course '{course.Name}'";
            
            var enrollment = new Enrollment(course, this, grade);
            _enrollments.Add(enrollment);

            return "OK";
        }

        public void Disenroll(Course course)
        {
            Enrollment enrollment = _enrollments.FirstOrDefault(x => x.Course == course);

            if (enrollment == null)
                return;

            _enrollments.Remove(enrollment);
        }
    }
}
