﻿namespace PreservingEncapsulation.Module3
{
    public class Student
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public long FavoriteCourseId { get; set; }
    }
}
