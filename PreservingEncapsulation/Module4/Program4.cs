﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;

namespace PreservingEncapsulation.Module4
{
    public class Program4
    {
        public static void MainModule4()
        {
            string connectionString = GetConnectionString();

            // In Asp.NET Core:
            // services.AddScoped(_=> new SchoolContext(connectionString, useConsoleLogging)); // for custom DbContext, instead of registering default one with services.AddDbContext() method.
            using (var context = new SchoolContext(connectionString, true))
            {
                //Student student = context.Students.Find(1L); // because EF does not load navigation properties by default, we have to call Include().

                Student student = context.Students
                    .Include(x => x.FavoriteCourse)
                    .SingleOrDefault(x => x.Id == 1);
            }
        }

        private static string GetConnectionString()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            return configuration["ConnectionString"];
        }
    }
}
