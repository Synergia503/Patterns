﻿namespace PreservingEncapsulation.Module4
{
    public class Course
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
