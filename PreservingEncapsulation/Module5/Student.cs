﻿namespace PreservingEncapsulation.Module5
{
    // not sealed class is needed for EF Core Lazy Loading feature
    public class Student : Entity
    {
        public string Name { get; }
        public string Email { get; }

        // virtual properties are needed for EF Core Lazy Loading feature
        public virtual Course FavoriteCourse { get; }

        // protected/public parameterless constructor is needed for EF Core Lazy Loading feature
        protected Student()
        {
        }

        public Student(string name, string email, Course favoriteCourse)
            : this()
        {
            Name = name;
            Email = email;
            FavoriteCourse = favoriteCourse;
        }
    }
}
