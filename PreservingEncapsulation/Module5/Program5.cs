﻿using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace PreservingEncapsulation.Module5
{
    public class Program5
    {
        public static void MainModule5()
        {
            string connectionString = GetConnectionString();

            using (var context = new SchoolContext(connectionString, true))
            {
                Student student = context.Students.Find(1L);
                Course course = student.FavoriteCourse;

                Course course2 = context.Courses.SingleOrDefault(x => x.Id == 2);

                bool boolean = course == course2;

                bool boolean2 = course == Course.Chemistry;
            }
        }

        private static string GetConnectionString()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            return configuration["ConnectionString"];
        }
    }
}
