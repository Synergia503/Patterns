﻿using System;

namespace MSA.Aggregates
{
    public interface IAggregate 
    {
        Guid Id { get; }
    }
}
