﻿namespace MSA.Business
{
    public enum MatchState
    {
        ToBePlayed = 0,
        InProgress = 1,
        Finished = 2
    }
}