﻿using System;

namespace LSP
{
    //https://softwareengineering.stackexchange.com/questions/359542/is-it-ever-okay-to-violate-the-lsp
    //https://softwareengineering.stackexchange.com/questions/240980/does-this-decorator-implementation-violate-the-liskov-substitution-principle
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
