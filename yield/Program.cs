﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace yield
{
    class Program
    {
        //todo: 
        // https://stackoverflow.com/questions/58540156/updating-items-in-an-ienumerable
        // https://stackoverflow.com/questions/9104181/updating-an-item-property-within-ienumerable-but-the-property-doesnt-stay-set
        static void Main(string[] args)
        {
            // https://www.kenneth-truyers.net/2016/05/12/yield-return-in-c/
            var invoices = GetInvoices();
            DoubleAmounts(invoices);
            Console.WriteLine(invoices.First().Amount);

            foreach (var number in FilterWithYield(new List<int> { 1, 2, 3, 4, 5, 6 }))
            {
                Console.WriteLine(number);
            }
        }

        public static IEnumerable<int> FilterWithYield(List<int> numbers)
        {
            foreach (var number in numbers)
            {
                if (number > 3)
                {
                    yield return number;
                }
            }
        }

        static IEnumerable<Invoice> GetInvoices()
        {
            for (var i = 1; i < 11; i++)
                yield return new Invoice { Amount = i * 10 };
        }

        static void DoubleAmounts(IEnumerable<Invoice> invoices)
        {
            foreach (var invoice in invoices)
                invoice.Amount *= 2;
        }

        private class Invoice
        {
            public int Amount { get; internal set; }
        }
    }
}