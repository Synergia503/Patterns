﻿namespace Observer.WeatherStation.Observable
{
    public interface IObserver
    {
        void Update(float temperature, float humidity, float pressure);
    }
}