﻿namespace Observer.WeatherStation.Observable
{
    public interface IDisplayable
    {
        void Display();
    }
}