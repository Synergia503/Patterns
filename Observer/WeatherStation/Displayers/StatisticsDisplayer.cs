﻿using Observer.WeatherStation.Observable;
using static System.Console;

namespace Observer.WeatherStation.Displayers
{
    public class StatisticsDisplayer : IObserver, IDisplayable
    {
        private float _maxTemperature = 40.0f;
        private float _minTemperature = 20f;
        private float _temperatureSum = 0.0f;
        private int _readingsCount;
        private WeatherData _weatherData;

        public StatisticsDisplayer(WeatherData weatherData)
        {
            _weatherData = weatherData;
            weatherData.RegisterObserver(this);
        }

        public void Update(float temperature, float humidity, float pressure)
        {
            _temperatureSum += temperature;
            _readingsCount++;

            if (temperature > _maxTemperature)
            {
                _maxTemperature = temperature;
            }

            if (temperature < _minTemperature)
            {
                _minTemperature = temperature;
            }

            Display();
        }

        public void Display()
        {
            WriteLine($"\nStatistics of temperature:\n\t-Average: {_temperatureSum / _readingsCount}\n\t-Max: {_maxTemperature}\n\t-Min: {_minTemperature}");
        }
    }
}