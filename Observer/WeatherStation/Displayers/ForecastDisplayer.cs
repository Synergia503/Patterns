﻿using Observer.WeatherStation.Observable;
using static System.Console;

namespace Observer.WeatherStation.Displayers
{
    public class ForecastDisplayer : IObserver, IDisplayable
    {
        private float _currentPressure = 29.92f;
        private float _lastPressure;
        private WeatherData _weatherData;

        public ForecastDisplayer(WeatherData weatherData)
        {
            _weatherData = weatherData;
            weatherData.RegisterObserver(this);
        }

        public void Update(float temperature, float humidity, float pressure)
        {
            _lastPressure = _currentPressure;
            _currentPressure = pressure;
            Display();
        }

        public void Display()
        {
            WriteLine("\nForecast: ");
            if (_currentPressure > _lastPressure)
            {
                WriteLine("Improving weather on the way!");
            }
            else if (_currentPressure == _lastPressure)
            {
                WriteLine("More of the same");
            }
            else if (_currentPressure < _lastPressure)
            {
                WriteLine("Watch out for cooler, rainy weather");
            }
        }
    }
}