﻿using Observer.WeatherStation.Observable;
using static System.Console;

namespace Observer.WeatherStation.Displayers
{
    public class CurrentConditionsDisplayer : IObserver, IDisplayable
    {
        private float _temperature;
        private float _humidity;
        private float _pressure;
        private ISubject _weatherData;

        public CurrentConditionsDisplayer(ISubject weatherData)
        {
            _weatherData = weatherData;
            weatherData.RegisterObserver(this);
        }

        public void Update(float temperature, float humidity, float pressure)
        {
            _temperature = temperature;
            _humidity = humidity;
            _pressure = pressure;
            Display();
        }

        public void Display()
        {
            WriteLine($"\nCurrent conditions:\n\t-temperature: {_temperature}\n\t-humidity: {_humidity}\n\t-pressure: {_pressure}");
        }
    }
}