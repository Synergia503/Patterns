﻿using Observer.WeatherStation.Displayers;

namespace Observer.WeatherStation
{
    public static class WeatherStationRunner
    {
        public static void Run()
        {
            var weatherData = new WeatherData();
            var currentConditionsDisplayer = new CurrentConditionsDisplayer(weatherData);
            var statisticsDisplayer = new StatisticsDisplayer(weatherData);
            var forecastDisplayer = new ForecastDisplayer(weatherData);
            var headIndexDisplayer = new HeatIndexDisplayer(weatherData);

            weatherData.SetMeasurements(26.6f, 65f, 1013.1f);
            weatherData.SetMeasurements(27.7f, 70f, 997.0f);
            weatherData.SetMeasurements(25.5f, 90f, 997.0f);
            weatherData.SetMeasurements(28.1f, 83f, 1003.5f);
        }
    }
}