﻿using Observer.StockTicker.Domain;
using System;

namespace Observer.StockTicker.ObserverWithEvents
{
    public class StockChangeEventArgs : EventArgs
    {
        public StockChangeEventArgs(Stock s)
        {
            Stock = s;
        }

        public Stock Stock { get; set; }
    }
}