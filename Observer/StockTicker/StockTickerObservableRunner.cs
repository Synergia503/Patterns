﻿using Observer.StockTicker.Domain;

namespace Observer.StockTicker
{
    public static class StockTickerObservableRunner
    {
        public static void Run()
        {
            var stockTicker = new Observable.StockTicker();
            var googleMonitor = new Observable.GoogleMonitor();
            var microsoftMonitor = new Observable.MicrosoftMonitor();
            using (stockTicker.Subscribe(googleMonitor))
            {
                using (stockTicker.Subscribe(microsoftMonitor))
                {
                    foreach (Stock stock in SampleData.GetNext())
                    {
                        stockTicker.Stock = stock;
                    }
                }
            }
        }
    }
}