﻿using System;

namespace Observer.StockTicker.Domain
{
    public class GoogleMonitor
    {
        public GoogleMonitor(ObserverWithEvents.StockTicker stockTicker)
        {
            stockTicker.StockChange += new EventHandler<ObserverWithEvents.StockChangeEventArgs>(StockTickerStockChange);
        }

        void StockTickerStockChange(object sender, ObserverWithEvents.StockChangeEventArgs e)
        {
            CheckFilter(e.Stock);
        }

        private void CheckFilter(Stock value)
        {
            if (value.Symbol == "GOOG")
            {
                Console.WriteLine("Google's new price is: {0}", value.Price);
            }
        }
    }
}