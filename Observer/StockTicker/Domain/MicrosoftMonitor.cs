﻿using System;

namespace Observer.StockTicker.Domain
{
    public class MicrosoftMonitor
    {
        public MicrosoftMonitor(ObserverWithEvents.StockTicker stockTicker)
        {
            stockTicker.StockChange += new EventHandler<ObserverWithEvents.StockChangeEventArgs>(StockTickerStockChange);
        }

        void StockTickerStockChange(object sender, ObserverWithEvents.StockChangeEventArgs e)
        {
            CheckFilter(e.Stock);
        }

        private void CheckFilter(Stock value)
        {
            if (value.Symbol == "MSFT" && value.Price > 10.00m)
            {
                Console.WriteLine("Microsoft has reached the target price: {0}", value.Price);
            }
        }
    }
}