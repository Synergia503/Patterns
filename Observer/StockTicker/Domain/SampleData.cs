﻿using System.Collections.Generic;

namespace Observer.StockTicker.Domain
{
    public class SampleData
    {
        private static decimal[] _samplePrices = new decimal[] { 10.00m, 10.25m, 555.55m, 9.50m, 9.03m, 500.00m, 499.99m, 10.10m };
        private static string[] _sampleStocks = new string[] { "MSFT", "MSFT", "GOOG", "MSFT", "MSFT", "GOOG", "GOOG", "MSFT" };

        public static IEnumerable<Stock> GetNext()
        {
            for (int i = 0; i < _samplePrices.Length; i++)
            {
                Stock s = new Stock
                {
                    Symbol = _sampleStocks[i],
                    Price = _samplePrices[i]
                };

                yield return s;
            }
        }
    }
}