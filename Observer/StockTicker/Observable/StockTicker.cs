﻿using Observer.StockTicker.Domain;
using System;
using System.Collections.Generic;

namespace Observer.StockTicker.Observable
{
    public class StockTicker : IObservable<Stock>
    {
        ISet<IObserver<Stock>> observers = new HashSet<IObserver<Stock>>();

        private Stock stock;

        public Stock Stock
        {
            get { return stock; }
            set
            {
                stock = value;
                Notify(stock);
            }
        }

        private void Notify(Stock stock)
        {
            foreach (IObserver<Stock> observer in observers)
            {
                if (stock.Symbol == null || stock.Price < 0)
                {
                    observer.OnError(new Exception("Bad Stock Data"));
                }
                else
                {
                    observer.OnNext(stock);
                }
            }
        }

        private void Stop()
        {
            foreach (IObserver<Stock> observer in observers)
            {
                if (observers.Contains(observer))
                {
                    observer.OnCompleted();
                }
            }

            observers.Clear();
        }

        public IDisposable Subscribe(IObserver<Stock> observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }

            return new Unsubscriber(observers, observer);
        }

        private class Unsubscriber : IDisposable
        {
            private ISet<IObserver<Stock>> _observers;
            private IObserver<Stock> _observer;

            public Unsubscriber(ISet<IObserver<Stock>> observers, IObserver<Stock> observer)
            {
                _observers = observers;
                _observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                {
                    _observers.Remove(_observer);
                }
            }
        }
    }
}