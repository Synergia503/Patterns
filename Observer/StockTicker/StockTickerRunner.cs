﻿using Observer.StockTicker.Domain;

namespace Observer.StockTicker
{
    public static class StockTickerRunner
    {
        public static void Run()
        {
            var stockTicker = new ObserverWithEvents.StockTicker();
            GoogleMonitor googleMonitor = new GoogleMonitor(stockTicker);
            MicrosoftMonitor microsoftMonitor = new MicrosoftMonitor(stockTicker);

            foreach (Stock stock in SampleData.GetNext())
            {
                stockTicker.Stock = stock;
            }
        }
    }
}