﻿using Observer.PriceWatcher.Domain;

namespace Observer.PriceWatcher
{
    public class PriceWatcherRunner
    {
        private BigStorePrice _bigStorePrice;

        public void Run()
        {
            _bigStorePrice = new BigStorePrice { ProductPrice = 1.25m };
            var observer1 = new Observer.PriceWatcher("Observer 1");
            _bigStorePrice.Subscribe(observer1);
            observer1.PriceChanged += Observer1PriceChnged;

            var observer2 = new Observer.PriceWatcher("Observer 2");
            _bigStorePrice.Subscribe(observer2);
            observer2.PriceChanged += Observer2PriceChnged;

            _bigStorePrice.ProductPrice = 55m;
            _bigStorePrice.ProductPrice = 155m;
            _bigStorePrice.ProductPrice = 255m;
        }

        public void Observer1PriceChnged(object sender, decimal e)
        {
            System.Console.WriteLine($"{((Observer.PriceWatcher)sender).Name} with price: {e.ToString("C")}");
        }

        public void Observer2PriceChnged(object sender, decimal e)
        {
            System.Console.WriteLine($"{((Observer.PriceWatcher)sender).Name} with price: {e.ToString("C")}");
        }
    }
}