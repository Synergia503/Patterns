﻿using Observer.PriceWatcher.Domain;

namespace Observer.PriceWatcher.Observer
{
    public abstract class PriceObserver
    {
        public abstract void Update(Price price);
    }
}