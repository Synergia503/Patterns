﻿using Observer.PriceWatcher.Domain;
using System;

namespace Observer.PriceWatcher.Observer
{
    public class PriceWatcher : PriceObserver
    {
        private string _name;
        private decimal _price;
        public event EventHandler<decimal> PriceChanged = null;

        public PriceWatcher(string name)
        {
            _name = name;
        }

        public string Name => _name;

        public override void Update(Price priceToWatch)
        {
            _price = priceToWatch.ProductPrice;
            PriceChanged?.Invoke(this, _price);
        }
    }
}