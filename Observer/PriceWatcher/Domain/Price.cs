﻿using Observer.PriceWatcher.Observer;
using System.Collections.Generic;

namespace Observer.PriceWatcher.Domain
{
    public abstract class Price
    {
        private ISet<PriceObserver> _priceObservers = new HashSet<PriceObserver>();
        private decimal _productPrice;
        public decimal ProductPrice
        {
            get
            {
                return _productPrice;
            }
            set
            {
                if (_productPrice != value)
                {
                    _productPrice = value;
                    Notify();
                }
            }
        }

        public void Subscribe(PriceObserver observer)
        {
            _priceObservers.Add(observer);
        }

        public void Unregister(PriceObserver observer)
        {
            _priceObservers.Remove(observer);
        }

        public void Notify()
        {
            foreach (PriceObserver observer in _priceObservers)
            {
                observer.Update(this);
            }
        }
    }
}