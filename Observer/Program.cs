﻿using Observer.PriceWatcher;
using Observer.StockTicker;
using Observer.WeatherStation;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            WeatherStationRunner.Run();
            var priceWatcherRunner = new PriceWatcherRunner();
            priceWatcherRunner.Run();
            StockTickerRunner.Run();
            StockTickerObservableRunner.Run();
        }
    }
}