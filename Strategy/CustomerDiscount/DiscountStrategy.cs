﻿namespace Strategy.CustomerDiscount
{
    public abstract class DiscountStrategy
    {
        protected double DiscountPercentage { get; set; }
        public abstract decimal ApplyDiscount(decimal sale);
    }
}