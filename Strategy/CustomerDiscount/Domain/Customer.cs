﻿namespace Strategy.CustomerDiscount.Domain
{
    public class Customer
    {
        private DiscountStrategy _discountStrategy;

        public void SetDiscountStrategy(DiscountStrategy discountStrategy)
        {
            _discountStrategy = discountStrategy;
        }

        public decimal ApplyDiscount(decimal sale)
        {
            return _discountStrategy.ApplyDiscount(sale);
        }
    }
}