﻿namespace Strategy.CustomerDiscount.DiscountService
{
    public class NoDiscount : DiscountStrategy
    {
        public NoDiscount()
        {
            DiscountPercentage = 0;
        }

        public override decimal ApplyDiscount(decimal sale)
        {
            return (decimal)(100 - DiscountPercentage) * sale / 100;
        }
    }
}