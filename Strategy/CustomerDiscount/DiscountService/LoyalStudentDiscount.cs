﻿namespace Strategy.CustomerDiscount.DiscountService
{
    public class LoyalStudentDiscount : DiscountStrategy
    {
        public LoyalStudentDiscount()
        {
            DiscountPercentage = 33;
        }

        public override decimal ApplyDiscount(decimal sale)
        {
            return (decimal)(100 - DiscountPercentage) * sale / 100;
        }
    }
}