﻿namespace Strategy.CustomerDiscount.DiscountService
{
    public class LoyalDiscount : DiscountStrategy
    {
        public LoyalDiscount()
        {
            DiscountPercentage = 15;
        }

        public override decimal ApplyDiscount(decimal sale)
        {
            return (decimal)(100 - DiscountPercentage) * sale / 100;
        }
    }
}