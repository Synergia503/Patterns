﻿namespace Strategy.CustomerDiscount.DiscountService
{
    public class StudentDiscount : DiscountStrategy
    {
        public StudentDiscount()
        {
            DiscountPercentage = 25;
        }

        public override decimal ApplyDiscount(decimal sale)
        {
            return (decimal)(100 - DiscountPercentage) * sale / 100;
        }
    }
}