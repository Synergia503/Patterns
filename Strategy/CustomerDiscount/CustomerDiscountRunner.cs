﻿using Strategy.CustomerDiscount.DiscountService;
using Strategy.CustomerDiscount.Domain;
using static System.Console;

namespace Strategy.CustomerDiscount
{
    public static class CustomerDiscountRunner
    {
        public static void Run()
        {
            var customer = new Customer();
            customer.SetDiscountStrategy(new NoDiscount());

            decimal price = 19.99m;

            WriteLine($"\nBase price: {price}");
            WriteLine($"Final price: {customer.ApplyDiscount(price).ToString("C")}");

            customer.SetDiscountStrategy(new StudentDiscount());
            WriteLine($"Final price: {customer.ApplyDiscount(price).ToString("C")}");

            customer.SetDiscountStrategy(new LoyalDiscount());
            WriteLine($"Final price: {customer.ApplyDiscount(price).ToString("C")}");

            customer.SetDiscountStrategy(new LoyalStudentDiscount());
            WriteLine($"Final price: {customer.ApplyDiscount(price).ToString("C")}");
        }
    }
}