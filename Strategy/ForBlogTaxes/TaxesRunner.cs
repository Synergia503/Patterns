﻿using Strategy.ForBlogTaxes.InterfacesImplementations.ChildTaxCredit;
using Strategy.ForBlogTaxes.InterfacesImplementations.Tax;
using Strategy.ForBlogTaxes.TaxImplementations;

namespace Strategy.ForBlogTaxes
{
    public static class TaxesRunner
    {
        public static void Run()
        {
            Tax incomeTax = new CommonIncomeTax(50000, 2);
            incomeTax.ShowTaxesAmounts();
            incomeTax.SetTaxable(new TaxThresholdC());
            incomeTax.SetChildTaxCreditable(new AdoptedChildren());
            incomeTax.ShowTaxesAmounts();

            Tax soleTraderTax = new SoleTraderTax(150000, 3);
            soleTraderTax.ShowTaxesAmounts();
            soleTraderTax.SetTaxable(new TaxThresholdA());
            soleTraderTax.SetChildTaxCreditable(new DisabledChildren());
            soleTraderTax.ShowTaxesAmounts();

            Tax retirementTax = new RetirementTax(30000);
            retirementTax.ShowTaxesAmounts();
            retirementTax.SetTaxable(new TaxThresholdB());
            retirementTax.ShowTaxesAmounts();
            retirementTax.SetTaxable(new NoneTax());
            retirementTax.ShowTaxesAmounts();
        }
    }
}