﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.Tax
{
    public class NoneTax : ITaxable
    {
        public decimal CalculateTax(decimal income)
        {
            return 0m;
        }
    }
}