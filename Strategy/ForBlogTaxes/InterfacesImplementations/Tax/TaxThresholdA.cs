﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.Tax
{
    public class TaxThresholdA : ITaxable
    {
        public decimal CalculateTax(decimal income)
        {
            return 0.25m * income;
        }
    }
}