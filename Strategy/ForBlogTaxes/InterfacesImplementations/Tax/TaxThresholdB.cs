﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.Tax
{
    public class TaxThresholdB : ITaxable
    {
        public decimal CalculateTax(decimal income)
        {
            return 0.2m * income;
        }
    }
}