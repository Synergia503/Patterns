﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.Tax
{
    public class TaxThresholdC : ITaxable
    {
        public decimal CalculateTax(decimal income)
        {
            return 0.15m * income;
        }
    }
}