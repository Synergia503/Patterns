﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.ChildTaxCredit
{
    public class AdoptedChildren : IChildTaxCreditable
    {
        public decimal CalculateCredit(decimal income, int childrenCount)
        {
            return childrenCount * (0.1m * income);
        }
    }
}