﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.ChildTaxCredit
{
    public class Children : IChildTaxCreditable
    {
        public decimal CalculateCredit(decimal income, int childrenCount)
        {
            return childrenCount * (0.05m * income);
        }
    }
}