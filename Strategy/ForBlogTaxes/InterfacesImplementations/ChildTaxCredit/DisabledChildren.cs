﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes.InterfacesImplementations.ChildTaxCredit
{
    public class DisabledChildren : IChildTaxCreditable
    {
        public decimal CalculateCredit(decimal income, int childrenCount)
        {
            return childrenCount * (0.5m * income);
        }
    }
}