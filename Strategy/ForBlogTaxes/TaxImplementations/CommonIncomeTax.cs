﻿using Strategy.ForBlogTaxes.InterfacesImplementations.ChildTaxCredit;
using Strategy.ForBlogTaxes.InterfacesImplementations.Tax;
using System;

namespace Strategy.ForBlogTaxes.TaxImplementations
{
    public class CommonIncomeTax : Tax
    {
        private readonly decimal _income;
        private readonly int _childCount;

        public CommonIncomeTax(decimal amount, int childCount = 0)
        {
            _taxable = new TaxThresholdA();
            _childTaxCreditable = new Children();
            _income = amount;
            _childCount = childCount;
        }

        public override void ShowTaxesAmounts()
        {
            Console.WriteLine($"\nCommon income tax to be paid: {_taxable.CalculateTax(_income)}");
            Console.WriteLine($"Child tax credit to be gotten:{_childTaxCreditable.CalculateCredit(_income, _childCount)}");
        }
    }
}