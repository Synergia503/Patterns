﻿using Strategy.ForBlogTaxes.InterfacesImplementations.ChildTaxCredit;
using Strategy.ForBlogTaxes.InterfacesImplementations.Tax;
using System;

namespace Strategy.ForBlogTaxes.TaxImplementations
{
    public class SoleTraderTax : Tax
    {
        private readonly decimal _income;
        private readonly int _childCount;

        public SoleTraderTax(decimal amount, int childCount = 0)
        {
            _taxable = new TaxThresholdB();
            _childTaxCreditable = new Children();
            _income = amount;
            _childCount = childCount;
        }

        public override void ShowTaxesAmounts()
        {
            Console.WriteLine($"\nSole trader income tax to be paid: {_taxable.CalculateTax(_income)}");
            Console.WriteLine($"Child tax credit to be gotten:{_childTaxCreditable.CalculateCredit(_income, _childCount)}");
        }
    }
}