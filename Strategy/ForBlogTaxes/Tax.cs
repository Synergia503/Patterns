﻿using Strategy.ForBlogTaxes.Interfaces;

namespace Strategy.ForBlogTaxes
{
    public abstract class Tax
    {
        protected ITaxable _taxable;
        protected IChildTaxCreditable _childTaxCreditable;

        protected Tax()
        {

        }

        protected decimal CalculateIncomeTax(decimal income)
        {
            return _taxable.CalculateTax(income);
        }

        protected decimal CalculateChildTaxCredit(decimal income, int childrenCount)
        {
            return _childTaxCreditable.CalculateCredit(income, childrenCount);
        }

        public abstract void ShowTaxesAmounts();

        public void SetTaxable(ITaxable taxable)
        {
            _taxable = taxable;
        }

        public void SetChildTaxCreditable(IChildTaxCreditable childTaxCreditable)
        {
            _childTaxCreditable = childTaxCreditable;
        }
    }
}