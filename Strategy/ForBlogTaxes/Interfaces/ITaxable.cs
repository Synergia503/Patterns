﻿namespace Strategy.ForBlogTaxes.Interfaces
{
    public interface ITaxable
    {
        decimal CalculateTax(decimal income);
    }
}