﻿namespace Strategy.ForBlogTaxes.Interfaces
{
    public interface IChildTaxCreditable
    {
        decimal CalculateCredit(decimal income, int childrenCount);
    }
}