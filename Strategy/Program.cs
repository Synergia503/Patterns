﻿using Strategy.CustomerDiscount;
using Strategy.Ducks;
using Strategy.ForBlogTaxes;
using Strategy.ShippingCostCalculator;

namespace Strategy
{
    public static class Program
    {
        static void Main(string[] args)
        {
            DucksRunner.Run();
            TaxesRunner.Run();
            CostCalculationRunner.Run();
            CostCalculationFuncsRunner.RunWithFuncs();
            CustomerDiscountRunner.Run();
        }
    }
}