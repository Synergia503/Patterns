﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks
{
    public abstract class Duck
    {
        protected IFlyable _flyable;
        protected IQuackable _quackable;

        protected Duck()
        {

        }

        public void DoFly()
        {
            _flyable.Fly();
        }

        public void DoQuack()
        {
            _quackable.Quack();
        }

        public abstract void Show();

        public void Swim()
        {
            Console.WriteLine("I'm swimming");
        }

        public void SetFlyable(IFlyable flyable)
        {
            _flyable = flyable;
        }

        public void SetQuackable(IQuackable quackable)
        {
            _quackable = quackable;
        }
    }
}