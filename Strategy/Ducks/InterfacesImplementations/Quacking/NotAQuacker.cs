﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks.InterfacesImplementations.Quacking
{
    public class NotAQuacker : IQuackable
    {
        public void Quack()
        {
            Console.WriteLine("I'm not quacking...");
        }
    }
}