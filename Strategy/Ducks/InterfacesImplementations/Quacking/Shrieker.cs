﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks.InterfacesImplementations.Quacking
{
    public class Shrieker : IQuackable
    {
        public void Quack()
        {
            Console.WriteLine("I'm shrieking...");
        }
    }
}