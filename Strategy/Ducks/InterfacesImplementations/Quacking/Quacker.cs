﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks.InterfacesImplementations.Quacking
{
    public class Quacker : IQuackable
    {
        public void Quack()
        {
            Console.WriteLine("I'm quacking...");
        }
    }
}