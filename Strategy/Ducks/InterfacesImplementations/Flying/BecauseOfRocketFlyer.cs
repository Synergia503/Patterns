﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks.InterfacesImplementations.Flying
{
    public class BecauseOfRocketFlyer : IFlyable
    {
        public void Fly()
        {
            Console.WriteLine("I'm flying with rocket...");
        }
    }
}