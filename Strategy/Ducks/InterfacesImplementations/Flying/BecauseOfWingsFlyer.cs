﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks.InterfacesImplementations.Flying
{
    public class BecauseOfWingsFlyer : IFlyable
    {
        public void Fly()
        {
            Console.WriteLine("I'm flying...");
        }
    }
}