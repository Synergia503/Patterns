﻿using Strategy.Ducks.Interfaces;
using System;

namespace Strategy.Ducks.InterfacesImplementations.Flying
{
    public class NotAFlyer : IFlyable
    {
        public void Fly()
        {
            Console.WriteLine("I'm not flying...");
        }
    }
}