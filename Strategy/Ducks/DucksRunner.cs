﻿using Strategy.Ducks.DuckImplementations;
using Strategy.Ducks.InterfacesImplementations.Flying;

namespace Strategy.Ducks
{
    public static class DucksRunner
    {
        public static void Run()
        {
            Duck duck = new WildDuck();
            duck.DoFly();
            duck.DoQuack();
            Duck modelDuck = new DuckModel();
            modelDuck.DoFly();
            modelDuck.SetFlyable(new BecauseOfRocketFlyer());
            modelDuck.DoFly();
        }
    }
}