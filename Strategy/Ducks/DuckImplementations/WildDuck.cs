﻿using Strategy.Ducks.InterfacesImplementations.Flying;
using Strategy.Ducks.InterfacesImplementations.Quacking;
using System;

namespace Strategy.Ducks.DuckImplementations
{
    public class WildDuck : Duck
    {
        public WildDuck()
        {
            _flyable = new BecauseOfWingsFlyer();
            _quackable = new Quacker();
        }

        public override void Show()
        {
            Console.WriteLine("I'm wild duck.");
        }
    }
}