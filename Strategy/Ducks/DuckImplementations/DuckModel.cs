﻿using Strategy.Ducks.InterfacesImplementations.Flying;
using Strategy.Ducks.InterfacesImplementations.Quacking;
using System;

namespace Strategy.Ducks.DuckImplementations
{
    public class DuckModel : Duck
    {
        public DuckModel()
        {
            _flyable = new NotAFlyer();
            _quackable = new Quacker();
        }

        public override void Show()
        {
            Console.WriteLine("I'm duck model.");
        }
    }
}