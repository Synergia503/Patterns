﻿namespace Strategy.Ducks.Interfaces
{
    public interface IQuackable
    {
        void Quack();
    }
}