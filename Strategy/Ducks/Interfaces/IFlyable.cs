﻿namespace Strategy.Ducks.Interfaces
{
    public interface IFlyable
    {
        void Fly();
    }
}