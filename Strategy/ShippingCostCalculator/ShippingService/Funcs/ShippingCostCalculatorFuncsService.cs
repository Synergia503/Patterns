﻿using Strategy.ShippingCostCalculator.Domain;
using System;

namespace Strategy.ShippingCostCalculator.ShippingService.Funcs
{
    public class ShippingCostCalculatorFuncsService
    {
        private readonly Func<Order, double> _strategy;

        public ShippingCostCalculatorFuncsService(Func<Order, double> strategy)
        {
            _strategy = strategy;
        }

        public ShippingCostCalculatorFuncsService()
        {
        }

        public double Calculate(Order order)
        {
            return _strategy(order);
        }

        public double CalculateWithFuncParam(Order order, Func<Order, double> strategy)
        {
            return strategy(order);
        }
    }
}