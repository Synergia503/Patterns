﻿using Strategy.ShippingCostCalculator.Domain;

namespace Strategy.ShippingCostCalculator.ShippingService
{
    public interface IShippingCostStrategy
    {
        double Calculate(Order order);
    }
}