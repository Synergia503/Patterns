﻿using Strategy.ShippingCostCalculator.Domain;

namespace Strategy.ShippingCostCalculator.ShippingService
{
    public class ShippingCostCalculatorService
    {
        readonly IShippingCostStrategy _shippingCostStrategy;

        public ShippingCostCalculatorService(IShippingCostStrategy shippingCostStrategy)
        {
            _shippingCostStrategy = shippingCostStrategy;
        }

        public double CalculateShippingCost(Order order)
        {
            return _shippingCostStrategy.Calculate(order);
        }
    }
}