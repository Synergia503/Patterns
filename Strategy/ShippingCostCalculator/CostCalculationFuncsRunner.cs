﻿using Strategy.ShippingCostCalculator.Domain;
using Strategy.ShippingCostCalculator.ShippingService.Funcs;
using System;
using static System.Console;

namespace Strategy.ShippingCostCalculator
{
    public static class CostCalculationFuncsRunner
    {
        public static void RunWithFuncs()
        {
            Func<Order, double> fedExStrategy = CalculateForFedEx;
            Func<Order, double> upsStrategy = delegate (Order order) { return 4.25d; };
            Func<Order, double> uspsStrategy = order => 3.00d;

            var theOrder = new Order()
            {
                Destination = new Address() { City = "Berlin" },
                Origin = new Address() { City = "Madrid" }
            };

            var costFuncService = new ShippingCostCalculatorFuncsService(fedExStrategy);
            WriteLine($"FedEx: {costFuncService.Calculate(theOrder)}");

            costFuncService = new ShippingCostCalculatorFuncsService(upsStrategy);
            WriteLine($"UPS: {costFuncService.Calculate(theOrder)}");

            costFuncService = new ShippingCostCalculatorFuncsService(uspsStrategy);
            WriteLine($"USPS: {costFuncService.Calculate(theOrder)}");

            WriteLine($"FedEx (with func as param): {costFuncService.CalculateWithFuncParam(theOrder, fedExStrategy)}");

            WriteLine($"UPS (with func as param): {costFuncService.CalculateWithFuncParam(theOrder, upsStrategy)}");

            WriteLine($"USPS (with func as param): {costFuncService.CalculateWithFuncParam(theOrder, (o) => 3.00d)}");
        }

        internal static double CalculateForFedEx(Order order)
        {
            return 5.00d;
        }
    }
}