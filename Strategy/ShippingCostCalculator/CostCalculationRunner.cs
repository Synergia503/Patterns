﻿using Strategy.ShippingCostCalculator.Domain;
using Strategy.ShippingCostCalculator.ShippingService;
using static System.Console;

namespace Strategy.ShippingCostCalculator
{
    public static class CostCalculationRunner
    {
        public static void Run()
        {
            var order = new Order()
            {
                Destination = new Address() { City = "Berlin" },
                Origin = new Address() { City = "Madrid" }
            };

            WriteLine();

            var costService = new ShippingCostCalculatorService(new FedExShippingCostStrategy());
            WriteLine($"FedEx: {costService.CalculateShippingCost(order)}");

            costService = new ShippingCostCalculatorService(new UPSShippingCostStrategy());
            WriteLine($"UPS: {costService.CalculateShippingCost(order)}");

            costService = new ShippingCostCalculatorService(new USPSShippingCostStrategy());
            WriteLine($"USPS: {costService.CalculateShippingCost(order)}");
        }
    }
}